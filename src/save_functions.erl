%% @author Daniel
%% @doc @todo Add description to save_functions.


-module(save_functions).

%% ====================================================================
%% API functions
%% ====================================================================
-export([build_save_message/5]).
-include("base_records.hrl").

%%base attribute, biography or schedule converted to JSON:
build_save_message(AtrBioOrSched,JsonMsg,[], HolonName, HolonType)->
	Temp1 = string:concat("{\"HolonName\":\"", HolonName),
	Temp2 = string:concat(Temp1, "\",\"HolonType\":\""),
	Temp21 = string:concat(Temp2, HolonType),
	case AtrBioOrSched of 
		atr->
			Temp3 = string:concat(Temp21, "\",\"Attributes\":{");
		_->
			Temp3 = string:concat(Temp21, "\",\"Activities\":{")
	end,
	
	Temp4 = string:concat(Temp3, JsonMsg),
	FinalJsonMsg = string:concat(Temp4, "}}!"),%exclamation mark to indicate to python end of message
	FinalJsonMsg;
build_save_message(AtrBioExeOrSched,JsonMsg,BaseContent, HolonName, HolonType)->
	case string:equal(JsonMsg, "") of
		true->Temp = "";
		_->Temp = string:concat(JsonMsg, ",")
	end,
	[H|T] = BaseContent,
	case AtrBioExeOrSched of
		atr->
			NewJson = string:concat(Temp, atrToJson(H));
		_->
			NewJson = string:concat(Temp,actToJson(AtrBioExeOrSched, H))		
	end,
	build_save_message(AtrBioExeOrSched,NewJson,T, HolonName, HolonType).

%% ====================================================================
%% Internal functions
%% ====================================================================


%%Attribute conversion functions:
atrToJson(ATR)->
	%%%%Even though there are checks here that convert atoms to strings, the handle calls prevent non strings. These checks are however still in place, in case a future developer decides to allow atoms
	case is_atom(ATR#base_attribute.id) of
		true->
			AtrId = atom_to_list(ATR#base_attribute.id);
		_->
			AtrId = ATR#base_attribute.id
	end,
	case is_atom(ATR#base_attribute.type) of
		true->
			AtrType = atom_to_list(ATR#base_attribute.type);
		_->
			AtrType = ATR#base_attribute.type
	end,
	case is_atom(ATR#base_attribute.context) of
		true->
			AtrContext = atom_to_list(ATR#base_attribute.context);
		_->
			AtrContext = ATR#base_attribute.context
	end,
	AtrIdWithoutSpace = string:replace(AtrId, " ", "_"),
	Temp = string:concat("\"", AtrIdWithoutSpace),
	Temp1 = string:concat(Temp, "\":{\"id\":\""),
	Temp2 = string:concat(Temp1, AtrId),
	Temp21 = string:concat(Temp2, "\",\"type\":\""),
	Temp22 = string:concat(Temp21, AtrType),
	Temp3 = string:concat(Temp22, "\",\"context\":\""),
	Temp4 = string:concat(Temp3, AtrContext),
	Temp5 = string:concat(Temp4, "\",\"value\":"),
	Temp6 = string:concat(Temp5, erlangToJsonDataType(ATR#base_attribute.value)),
	Final = string:concat(Temp6, "}"),
	Final.


erlangToJsonDataType(Value)->
	%%this function will ensure Value is one of the following: true, false, number, string
	case is_boolean(Value) of
		true->
			ReturnString = atom_to_list(Value);
		_->
		case is_float(Value) of
			true->
				ReturnString = float_to_list(Value);
			_->
			case is_integer(Value) of
				true->
					ReturnString = integer_to_list(Value);
				_->

					case io_lib:printable_list(Value) of
						true->
							Temp = string:concat("\"",Value),
							ReturnString = string:concat(Temp, "\"");
						_->
							Bins = erlang:term_to_binary(Value),%%all records, lists, tuples and atoms (except true and false) will be binary values
							List = binary_to_list(Bins),%%binary is converted to list to be able to send with json as array
							ReturnString = lists:flatten(io_lib:format("~p",[List]))
					end
							
			end
		end
	end,
	ReturnString.

actToJson(BioExeOrSched,ACT)->
	%%%%Even though there are checks here that convert atoms to strings, the handle calls prevent non strings. These checks are however still in place, in case a future developer decides to allow atom
	case BioExeOrSched of 
		bio->
			case is_atom(ACT#stage3Activity.shell#activity_shell.id) of
				true->
					ActId = atom_to_list(ACT#stage3Activity.shell#activity_shell.id);
				_->
					ActId = string:replace(ACT#stage3Activity.shell#activity_shell.id," ", "-",all)
			end,
			case is_atom(ACT#stage3Activity.shell#activity_shell.type) of
				true->
					ActType = atom_to_list(ACT#stage3Activity.shell#activity_shell.type);
				_->
					ActType = ACT#stage3Activity.shell#activity_shell.type
			end,
			Tsched = erlang:integer_to_list(ACT#stage3Activity.shell#activity_shell.tsched),
			Tstart = erlang:integer_to_list(ACT#stage3Activity.shell#activity_shell.tstart),
			Tend = erlang:integer_to_list(ACT#stage3Activity.shell#activity_shell.tstart),
			Stage = erlang:integer_to_list(ACT#stage3Activity.shell#activity_shell.stage),
			S1data = erlangToJsonDataType(ACT#stage3Activity.s1data),
			S2data = erlangToJsonDataType(ACT#stage3Activity.s2data),
			S3data = erlangToJsonDataType(ACT#stage3Activity.s3data);
		sched->
			case is_atom(ACT#stage1Activity.shell#activity_shell.id) of
				true->
					ActId = atom_to_list(ACT#stage1Activity.shell#activity_shell.id);
				_->
					ActId = string:replace(ACT#stage1Activity.shell#activity_shell.id," ", "-", all)
			end,
			case is_atom(ACT#stage1Activity.shell#activity_shell.type) of
				true->
					ActType = atom_to_list(ACT#stage1Activity.shell#activity_shell.type);
				_->
					ActType = ACT#stage1Activity.shell#activity_shell.type
			end,
			Tsched = erlang:integer_to_list(ACT#stage1Activity.shell#activity_shell.tsched),
			Tstart = erlang:integer_to_list(ACT#stage1Activity.shell#activity_shell.tstart),
			Tend = erlang:integer_to_list(ACT#stage1Activity.shell#activity_shell.tstart),
			Stage = erlang:integer_to_list(ACT#stage1Activity.shell#activity_shell.stage),
			S1data = erlangToJsonDataType(ACT#stage1Activity.s1data),
			S2data = erlangToJsonDataType(none),
			S3data = erlangToJsonDataType(none);
		exe->
			case is_atom(ACT#stage2Activity.shell#activity_shell.id) of
				true->
					ActId = atom_to_list(ACT#stage2Activity.shell#activity_shell.id);
				_->
					ActId = ACT#stage2Activity.shell#activity_shell.id
			end,
			case is_atom(ACT#stage2Activity.shell#activity_shell.type) of
				true->
					ActType = atom_to_list(ACT#stage2Activity.shell#activity_shell.type);
				_->
					ActType = string:replace(ACT#stage2Activity.shell#activity_shell.type, " ", "-", all)
			end,
			Tsched = erlang:integer_to_list(ACT#stage2Activity.shell#activity_shell.tsched),
			Tstart = erlang:integer_to_list(ACT#stage2Activity.shell#activity_shell.tstart),
			Tend = erlang:integer_to_list(ACT#stage2Activity.shell#activity_shell.tstart),
			Stage = erlang:integer_to_list(ACT#stage2Activity.shell#activity_shell.stage),
			S1data = erlangToJsonDataType(ACT#stage2Activity.s1data),
			S2data = erlangToJsonDataType(ACT#stage2Activity.s2data),
			S3data = erlangToJsonDataType(none);
		_->
			ActId = "none",
			ActType = "none",
			Tsched = 0,
			Tstart = 0,
			Tend = 0,
			Stage = 0,
			S1data = erlangToJsonDataType(none),
			S2data = erlangToJsonDataType(none),
			S3data = erlangToJsonDataType(none),
			io:format("actToJson: unexpected BioExeOrSched")
	end,
	ActIdWithoutSpace = string:replace(ActId," ", "_", all),
	Temp = string:concat("\"", ActIdWithoutSpace),
	Temp1 = string:concat(Temp, "\":{\"id\":\""),
	Temp2 = string:concat(Temp1, ActId),
	Temp21 = string:concat(Temp2, "\",\"type\":\""),
	Temp22 = string:concat(Temp21, ActType),
	Temp3 = string:concat(Temp22, "\",\"tsched\":"),
	Temp4 = string:concat(Temp3, Tsched),
	Temp5 = string:concat(Temp4, ",\"tstart\":"),
	Temp6 = string:concat(Temp5, Tstart),
	Temp7 = string:concat(Temp6, ",\"tend\":"),
	Temp8 = string:concat(Temp7, Tend),
	Temp9 = string:concat(Temp8, ",\"stage\":"),
	Temp10 = string:concat(Temp9, Stage),
	Temp11 = string:concat(Temp10, ",\"s1data\":"),
	Temp12 = string:concat(Temp11, S1data),
	Temp13 = string:concat(Temp12, ",\"s2data\":"),
	Temp14 = string:concat(Temp13, S2data),
	Temp15 = string:concat(Temp14, ",\"s3data\":"),
	Temp16 = string:concat(Temp15, S3data),
	
	case BioExeOrSched of
		sched->
			Final = string:concat(Temp12, "}");
		exe->
			Final = string:concat(Temp14,"}");
		bio->
			Final = string:concat(Temp16, "}");
		_->
			Final = "error"
	end,
	Final.

