%% @author Daniel
%% @doc @todo Add description to get_functions.


-module(get_functions).
-include("base_records.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([build_get_message/3,jsonToActs/1, jsonToAtrs/1]).
build_get_message(FilterMap, HolonName, HolonType)->
	{ok,SearchBy} = maps:find("SearchBy", FilterMap),	
	Temp1 = string:concat("{\"HolonName\":\"", HolonName),
	Temp2 = string:concat(Temp1, "\",\"HolonType\":\""),
	Temp3 = string:concat(Temp2, HolonType),
	Temp4 = string:concat(Temp3, "\",\"SearchBy\":\""),	
	Temp5 = string:concat(Temp4, SearchBy),
	case string:equal(SearchBy, "id") or string:equal(SearchBy, "type") or string:equal(SearchBy, "context") of
		true->
			Temp6 = string:concat(Temp5, "\",\"SearchFor\":"),
			{ok,SearchFor} = maps:find("SearchFor", FilterMap),
			Temp7 = string:concat(Temp6, lists:flatten(io_lib:format("~p",[SearchFor]))),
			FinalJsonMsg = string:concat(Temp7, "}!");%exclamation mark to indicate to python end of message
		_->
			Temp6 = string:concat(Temp5, "\",\"SearchFor\":{\"types\":"),
			{ok, Type} = maps:find("type", FilterMap),
			Temp7 = string:concat(Temp6,lists:flatten(io_lib:format("~p",[Type]))),
			Temp8 = string:concat(Temp7, ",\"tsched\":"),
			{ok, Tsched}=maps:find("tsched",FilterMap),
			Temp9 = string:concat(Temp8, lists:flatten(io_lib:format("~p",[Tsched]))),
			Temp10 = string:concat(Temp9, ",\"tstart\":"),
			{ok, Tstart}=maps:find("tstart",FilterMap),
			Temp11 = string:concat(Temp10, lists:flatten(io_lib:format("~p",[Tstart]))),
			Temp12 = string:concat(Temp11, ",\"tend\":"),
			{ok, Tend}=maps:find("tend",FilterMap),
			Temp13 = string:concat(Temp12, lists:flatten(io_lib:format("~p",[Tend]))),
			FinalJsonMsg = string:concat(Temp13, "}}!")%exclamation mark to indicate to python end of message
	end,
	FinalJsonMsg.

jsonToAtrs(JsonMsg)->
	%%%%first remove all whitespaces, newlines and tabs
	Substrings = lists:flatten(string:replace(JsonMsg, " ", "", all)),
	Substrings2 = lists:flatten(string:replace(Substrings, "\n", "", all)),
	Substrings3 = lists:flatten(string:replace(Substrings2, "\t", "", all)),
	StartRemoved = string:trim(Substrings3, leading, "{"),
	EndBracketsRem = string:trim(StartRemoved, trailing, "}"),
	JsonList=string:split(EndBracketsRem, "},", all),
	AtrList = jsonListToAtrs([], JsonList),
	AtrList.

jsonListToAtrs(AtrList,[])->
	lists:reverse(AtrList);
jsonListToAtrs(AtrList, JsonList)->
	[H|T] = JsonList,
	JsonAtrTemp = string:find(H, "{"),
	JsonAtr = string:trim(JsonAtrTemp, leading, ":"),
	AtrProps = string:split(JsonAtr, ",\"", all),%%prevents split in arrays by adding \" in split and not just :
	[Jid, Jtype, Jcontext, Jvalue] = AtrProps,
	[_, IdS] = string:split(Jid, ":"),
	Id = jsonToErlang(IdS),
	[_, TypeS] = string:split(Jtype, ":"),
	Type = jsonToErlang(TypeS),
	[_,ContextS] = string:split(Jcontext,":"),
	Context = jsonToErlang(ContextS),
	[_,ValueS] = string:split(Jvalue, ":"),
	Value = jsonToErlang(ValueS),
	NewAtr = #base_attribute{id=Id,type=Type,context=Context,value=Value},
	NewAtrList = [NewAtr|AtrList],
	jsonListToAtrs(NewAtrList, T).
			

jsonToActs(JsonMsg)->
	%%%%first remove all whitespaces, newlines and tabs
	Substrings = lists:flatten(string:replace(JsonMsg, " ", "", all)),
	Substrings2 = lists:flatten(string:replace(Substrings, "\n", "", all)),
	Substrings3 = lists:flatten(string:replace(Substrings2, "\t", "", all)),
	StartRemoved = string:trim(Substrings3, leading, "{"),
	EndBracketsRem = string:trim(StartRemoved, trailing, "}"),
	JsonList=string:split(EndBracketsRem, "},", all),
	ActList = jsonListToActs([], JsonList),
	ActList.

jsonListToActs(ActList,[])->
	lists:reverse(ActList);
jsonListToActs(ActList, JsonList)->
	[H|T] = JsonList,
	JsonActTemp = string:find(H, "{"),
	JsonAct = string:trim(JsonActTemp, leading, ":"),
	ActProps = string:split(JsonAct, ",\"", all),%%prevents split in arrays by adding \" in split and not just :
	case length(ActProps) of
		7->
			[Jid, Jtype, Jtsched, Jtstart, Jtend, Jstage, Js1data] = ActProps,
			[_,S1dataStr] = string:split(Js1data, ":"),
			S1data = jsonToErlang(S1dataStr),
			Act = #stage1Activity{s1data = S1data};
		8->
			[Jid, Jtype, Jtsched, Jtstart, Jtend, Jstage, Js1data, Js2data] = ActProps,
			[_,S1dataStr] = string:split(Js1data, ":"),
			S1data = jsonToErlang(S1dataStr),
			[_,S2dataStr] = string:split(Js2data, ":"),
			S2data = jsonToErlang(S2dataStr),
			Act = #stage2Activity{s1data = S1data,s2data = S2data};
		9->
			[Jid, Jtype, Jtsched, Jtstart, Jtend, Jstage, Js1data, Js2data, Js3data] = ActProps,
			[_,S1dataStr] = string:split(Js1data, ":"),
			S1data = jsonToErlang(S1dataStr),
			[_,S2dataStr] = string:split(Js2data, ":"),
			S2data = jsonToErlang(S2dataStr),
			[_,S3dataStr] = string:split(Js3data, ":"),
			S3data = jsonToErlang(S3dataStr),
			Act = #stage3Activity{s1data = S1data, s2data = S2data, s3data=S3data};		
		_->
			[Jid, Jtype, Jtsched, Jtstart, Jtend, Jstage] = ["error","error","error","error","error","error"],
			Act = error
	end,
	[_, IdS] = string:split(Jid, ":"),
	Id = jsonToErlang(IdS),
	[_, TypeS] = string:split(Jtype, ":"),
	Type = jsonToErlang(TypeS),
	[_,TschedS] = string:split(Jtsched,":"),
	Tsched = jsonToErlang(TschedS),
	[_,TstartS] = string:split(Jtstart, ":"),
	Tstart = jsonToErlang(TstartS),
	[_,TendS] = string:split(Jtend, ":"),
	Tend = jsonToErlang(TendS),
	[_,StageS]=string:split(Jstage, ":"),
	Stage = jsonToErlang(StageS),
	Shell = #activity_shell{id = Id, type = Type, tsched = Tsched, tstart = Tstart, tend = Tend, stage = Stage},
	case length(ActProps) of
		7->
			NewAct = Act#stage1Activity{shell = Shell};
		8->
			NewAct = Act#stage2Activity{shell = Shell};
		9->
			NewAct = Act#stage3Activity{shell = Shell};
		_->
			NewAct = error
	end,
	NewActList = [NewAct|ActList],
	jsonListToActs(NewActList, T).

jsonToErlang(Str)->
	case (string:equal(Str, "true") or string:equal(Str, "false")) of
		true->
			Return = erlang:list_to_atom(Str);
		_->
		case string:to_float(Str) of
			{error,_}->
				case string:to_integer(Str) of
				{error,_}->
					case string:find(Str, "[") of%%%%%%continue here
						nomatch->
							Return = string:trim(Str, both, "\"");
						_->
						case string:find(Str, "]") of
							nomatch->
								Return = string:trim(Str, both, "\"");
							_->
								Temp=string:trim(Str, both, "["),
								Temp2=string:trim(Temp, both, "]"),
								StrList = string:split(Temp2, ",", all),
								IntList=strToIntList([],StrList),
								BinList = erlang:list_to_binary(IntList),
								Return = erlang:binary_to_term(BinList)
						end
					end;
				{Int,_}->Return = Int
				end;					
			{Float,_}->Return = Float
		end
	end,
	Return.
strToIntList(IntList,[])->
	IntList;
strToIntList(IntList,StrList)->
	[H|T] = StrList,
	{NewH,_} = string:to_integer(H),	
	NewIntL = lists:append(IntList, [NewH]),
	strToIntList(NewIntL, T).
%% ====================================================================
%% Internal functions
%% ====================================================================
