%% @author Daniel
%% @doc @todo Add description to test_cloud.


-module(test_google_sheet).
-include("base_records.hrl").


%% ====================================================================
%% API functions
%% ====================================================================
-export([test_multiple_holon_requests/0,test_save_atr/0,test_save_bio/0,test_save_sched/0,test_save_exe/0, test_update_bio/0,test_update_sched/0,test_update_exe/0,test_get_atr/0, test_get_bio/0, test_get_sched/0, test_get_exe/0, test_get_act/0,test_delete_atr/0, test_delete_bio/0, test_delete_sched/0, test_delete_exe/0, test_delete_act/0]).
test_multiple_holon_requests()->
	{ok, PID} = google_sheet_storage:start_link(),
	Bio1 = #stage3Activity{shell = #activity_shell{id = "Act_W_202008311200",type = "Welding", tsched = 5, tstart = 6, tend = 7, stage = 3}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Bio2 = #stage3Activity{shell = #activity_shell{id = "Act_W_202008311300",type = "Welding", tsched = 10, tstart = 30, tend = 40, stage = 3}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Bio3 = #stage3Activity{shell = #activity_shell{id = "Act_G_202008311200",type = "Grinding", tsched = 50, tstart = 60, tend = 70, stage = 3}, s1data = 5},	
	Act5 = #stage3Activity{shell = #activity_shell{id = "E1",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Act6 = #stage3Activity{shell = #activity_shell{id = "E2",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Act7 = #stage3Activity{shell = #activity_shell{id = "E3",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = 5},	
	Act8 = #stage3Activity{shell = #activity_shell{id = "E4",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = "string"},
	gen_server:call(PID, {save_bio_activities, "Daniel", "Human", [Bio1,Bio2,Bio3, Act8, Act5, Act6, Act7]}),
	gen_server:call(PID, {save_bio_activities, "Dale", "Human", [Bio1,Bio2,Bio3, Act8, Act5, Act6, Act7]}),
	gen_server:call(PID, {save_bio_activities, "Karel", "Human", [Bio1,Bio2,Bio3, Act8, Act5, Act6, Act7]}),
	gen_server:call(PID, {save_bio_activities, "Daniel", "Human", [Bio1,Bio2,Bio3, Act8, Act5, Act6, Act7]}),
	gen_server:call(PID, {save_bio_activities, "Dale", "Human", [Bio1,Bio2,Bio3, Act8, Act5, Act6, Act7]}),
	gen_server:call(PID, {save_bio_activities, "Karel", "Cow", [Bio1,Bio2,Bio3, Act8, Act5, Act6, Act7]}).

test_update_bio()->
	{ok, PID} = google_sheet_storage:start_link(),
	Bio1 = #stage3Activity{shell = #activity_shell{id = "Act_W_202008311200",type = "Welding", tsched = 5, tstart = 6, tend = 7, stage = 3}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Bio2 = #stage3Activity{shell = #activity_shell{id = "Act_W_202008311300",type = "Welding", tsched = 10, tstart = 30, tend = 40, stage = 3}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Bio3 = #stage3Activity{shell = #activity_shell{id = "Act_G_202008311200",type = "Grinding", tsched = 50, tstart = 60, tend = 70, stage = 3}, s1data = 5},	
	Bio4 = #stage3Activity{shell = #activity_shell{id = "Act_G_202008311300",type = "Grinding", tsched = 100, tstart = 300, tend = 400, stage = 3}, s1data = "string"},	
	Act5 = #stage3Activity{shell = #activity_shell{id = "E1",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Act6 = #stage3Activity{shell = #activity_shell{id = "E2",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Act7 = #stage3Activity{shell = #activity_shell{id = "E3",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = 5},	
	Act8 = #stage3Activity{shell = #activity_shell{id = "E4",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = "string"},
	Reply = gen_server:call(PID, {update_bio_activities, "Daniel", "Human", [Bio1,Bio2,Bio3, Act8, Act5, Act6, Act7]}),
	io:format("\nExpecting: success\n"),
	case Reply of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR}->
					io:format("\nResult:"),
					io:format(SR);
				R->
					io:format(R)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {update_bio_activities, "Daniel", "Human", [Bio4]}),
	io:format("\nExpecting: success\n"),
	case Reply2 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR2}->
					io:format("\nResult:"),
					io:format(SR2);
				R2->
					io:format(R2)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {update_bio_activities, "Daniel", "Human", Bio4}),
	io:format("\nExpecting: invalid input arguments\n"),
	case Reply3 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR3}->
					io:format("\nResult:"),
					io:format(SR3);
				R3->
					io:format(R3)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply3)
	end,
	Reply4 = gen_server:call(PID, {update_bio_activities, "Daniel", "Human", ["invalidBio"]}),
	io:format("\nExpecting: invalid activities\n"),
	case Reply4 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR4}->
					io:format("\nResult:"),
					io:format(SR4);
				R4->
					io:format(R4)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply4)
	end.
test_update_sched()->
	{ok, PID} = google_sheet_storage:start_link(),
	Sched1 = #stage1Activity{shell = #activity_shell{id = "Act_W_202008311201",type = "Welding", tsched = 5, tstart = 6, tend = 7, stage = 1}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Sched2 = #stage1Activity{shell = #activity_shell{id = "Act_W_202008311301",type = "Welding", tsched = 10, tstart = 30, tend = 40, stage = 1}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Sched3 = #stage1Activity{shell = #activity_shell{id = "Act_G_202008311201",type = "Grinding", tsched = 50, tstart = 60, tend = 70, stage = 1}, s1data = 5},	
	Sched4 = #stage1Activity{shell = #activity_shell{id = "Act_G_202008311301",type = "Grinding", tsched = 100, tstart = 300, tend = 400, stage = 1}, s1data = "string"},	
	Act5 = #stage1Activity{shell = #activity_shell{id = "E1",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 1}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Act6 = #stage1Activity{shell = #activity_shell{id = "E2",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 1}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Act7 = #stage1Activity{shell = #activity_shell{id = "E3",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 1}, s1data = 5},	
	Act8 = #stage1Activity{shell = #activity_shell{id = "E4",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 1}, s1data = "string"},
	Reply = gen_server:call(PID, {update_sched_activities, "Daniel", "Human", [Sched1, Sched2, Sched3, Act5, Act6, Act7, Act8]}),
	io:format("\nExpecting: success\n"),
	case Reply of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR}->
					io:format("\nResult:"),
					io:format(SR);
				R->
					io:format(R)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {update_sched_activities, "Daniel", "Human", [Sched4]}),
	io:format("\nExpecting: success\n"),
	case Reply2 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR2}->
					io:format("\nResult:"),
					io:format(SR2);
				R2->
					io:format(R2)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {update_sched_activities, "Daniel", "Human", Sched4}),
	io:format("\nExpecting: invalid input arguments\n"),
	case Reply3 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR3}->
					io:format("\nResult:"),
					io:format(SR3);
				R3->
					io:format(R3)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply3)
	end,
	Reply4 = gen_server:call(PID, {update_sched_activities, "Daniel", "Human", [invalid_sched]}),
	io:format("\nExpecting: not valid activities\n"),
	case Reply4 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR4}->
					io:format("\nResult:"),
					io:format(SR4);
				R4->
					io:format(R4)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply4)
	end.
test_update_exe()->
	{ok, PID} = google_sheet_storage:start_link(),
	Exe1 = #stage2Activity{shell = #activity_shell{id = "Act_W_202008311202",type = "Welding", tsched = 5, tstart = 6, tend = 7, stage = 2}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Exe2 = #stage2Activity{shell = #activity_shell{id = "Act_W_202008311302",type = "Welding", tsched = 10, tstart = 30, tend = 40, stage = 2}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Exe3 = #stage2Activity{shell = #activity_shell{id = "Act_G_202008311202",type = "Grinding", tsched = 50, tstart = 60, tend = 70, stage = 2}, s1data = 5},	
	Exe4 = #stage2Activity{shell = #activity_shell{id = "Act_G_202008311302",type = "Grinding", tsched = 100, tstart = 300, tend = 400, stage = 2}, s1data = "string"},
	Act5 = #stage2Activity{shell = #activity_shell{id = "E1",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 2}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Act6 = #stage2Activity{shell = #activity_shell{id = "E2",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 2}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Act7 = #stage2Activity{shell = #activity_shell{id = "E3",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 2}, s1data = 5},	
	Act8 = #stage2Activity{shell = #activity_shell{id = "E4",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 2}, s1data = "string"},
	Sched5 = #stage1Activity{shell = #activity_shell{id = "Act_G_202008311305",type = "Grinding", tsched = 100, tstart = 300, tend = 400, stage = 2}, s1data = "string"},	
	Reply = gen_server:call(PID, {update_exe_activities, "Daniel", "Human", [Exe1, Exe2, Exe3, Act5, Act6, Act7, Act8]}),
	io:format("\nExpecting: success\n"),
	case Reply of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR}->
					io:format("\nResult:"),
					io:format(SR);
				R->
					io:format(R)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {update_exe_activities, "Daniel", "Human", [Exe4]}),
	io:format("\nExpecting: success\n"),
	case Reply2 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR2}->
					io:format("\nResult:"),
					io:format(SR2);
				R2->
					io:format(R2)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {update_exe_activities, "Daniel", "Human", Exe4}),
	io:format("\nExpecting: invalid input arguments\n"),
	case Reply3 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR3}->
					io:format("\nResult:"),
					io:format(SR3);
				R3->
					io:format(R3)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply3)
	end,
	Reply4 = gen_server:call(PID, {update_exe_activities, "Daniel", "Human", [Sched5]}),
	io:format("\nExpecting: not valid activities\n"),
	case Reply4 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR4}->
					io:format("\nResult:"),
					io:format(SR4);
				R4->
					io:format(R4)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply4)
	end.
test_save_atr()->
	{ok, PID} = google_sheet_storage:start_link(),
	Atr1 = #base_attribute{id = "WeldingSpeed", type = "Welding", context="cm/min", value = 7},
	Atr2 = #base_attribute{id = "WeldingQuality", type = "Welding", context = "Max = 10", value = 6},
	Atr3 = #base_attribute{id = "AvgHeartrate", type = "Physical", context = "bpm", value = 72},
	Atr4 = #base_attribute{id = "Languages", type = "Communicative", context = "Factory communications", value = ["English", "Afrikaans"]},
	Atr5 = #base_attribute{id = "Favorite food", type = "Banter", context = "For lunch", value = "Oxtail"},
	Reply = gen_server:call(PID, {save_attributes, "Daniel", "Human", [Atr1,Atr2,Atr3,Atr4]}),
	io:format("\nExpecting: success\nResult:"),
	case Reply of 
		{wait_for_reply_from, _}->
			receive
				{save_result, SR}->
					io:format(SR);
				R->
					io:format(R)
			end;
		_->
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {save_attributes, "Daniel", "Human", [Atr5]}),
	io:format("\nExpecting: success\nResult:"),
	case Reply2 of 
		{wait_for_reply_from, _}->
			receive
				{save_result, SR2}->
					io:format(SR2);
				R2->
					io:format(R2)
			end;
		_->
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {save_attributes, "Daniel", "Human", Atr5}),
	io:format("\nExpecting: error, Atr needs to be list\nResult:"),
	case Reply3 of 
		{wait_for_reply_from, _}->
			receive
				{save_result, SR3}->
					io:format(SR3);
				R3->
					io:format(R3)
			end;
		_->
			io:format(Reply3)
	end,
	Reply4 = gen_server:call(PID, {save_attributes, "Daniel", "Human", ["Atr"]}),
	io:format("\nExpecting: error, list does not contain valid attributes\nResult:"),
	case Reply4 of 
		{wait_for_reply_from, _}->
			receive
				{save_result, SR4}->
					io:format(SR4);
				R4->
					io:format(R4)
			end;
		_->
			io:format(Reply4)
	end.
test_save_bio()->
	{ok, PID} = google_sheet_storage:start_link(),
	Bio1 = #stage3Activity{shell = #activity_shell{id = "Act_W_202008311200",type = "Welding", tsched = 5, tstart = 6, tend = 7, stage = 3}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Bio2 = #stage3Activity{shell = #activity_shell{id = "Act_W_202008311300",type = "Welding", tsched = 10, tstart = 30, tend = 40, stage = 3}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Bio3 = #stage3Activity{shell = #activity_shell{id = "Act_G_202008311200",type = "Grinding", tsched = 50, tstart = 60, tend = 70, stage = 3}, s1data = 5},	
	Bio4 = #stage3Activity{shell = #activity_shell{id = "Act_G_202008311300",type = "Grinding", tsched = 100, tstart = 300, tend = 400, stage = 3}, s1data = "string"},	
	Act5 = #stage3Activity{shell = #activity_shell{id = "E1",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Act6 = #stage3Activity{shell = #activity_shell{id = "E2",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Act7 = #stage3Activity{shell = #activity_shell{id = "E3",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = 5},	
	Act8 = #stage3Activity{shell = #activity_shell{id = "E4",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 3}, s1data = "string"},
	Reply = gen_server:call(PID, {save_bio_activities, "Daniel", "Human", [Bio1,Bio2,Bio3, Act8, Act5, Act6, Act7]}),
	io:format("\nExpecting: success\n"),
	case Reply of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR}->
					io:format("\nResult:"),
					io:format(SR);
				R->
					io:format(R)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {save_bio_activities, "Daniel", "Human", [Bio4]}),
	io:format("\nExpecting: success\n"),
	case Reply2 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR2}->
					io:format("\nResult:"),
					io:format(SR2);
				R2->
					io:format(R2)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {save_bio_activities, "Daniel", "Human", Bio4}),
	io:format("\nExpecting: invalid input arguments\n"),
	case Reply3 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR3}->
					io:format("\nResult:"),
					io:format(SR3);
				R3->
					io:format(R3)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply3)
	end,
	Reply4 = gen_server:call(PID, {save_bio_activities, "Daniel", "Human", ["invalidBio"]}),
	io:format("\nExpecting: invalid activities\n"),
	case Reply4 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR4}->
					io:format("\nResult:"),
					io:format(SR4);
				R4->
					io:format(R4)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply4)
	end.
test_save_sched()->
	{ok, PID} = google_sheet_storage:start_link(),
	Sched1 = #stage1Activity{shell = #activity_shell{id = "Act_W_202008311201",type = "Welding", tsched = 5, tstart = 6, tend = 7, stage = 1}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Sched2 = #stage1Activity{shell = #activity_shell{id = "Act_W_202008311301",type = "Welding", tsched = 10, tstart = 30, tend = 40, stage = 1}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Sched3 = #stage1Activity{shell = #activity_shell{id = "Act_G_202008311201",type = "Grinding", tsched = 50, tstart = 60, tend = 70, stage = 1}, s1data = 5},	
	Sched4 = #stage1Activity{shell = #activity_shell{id = "Act_G_202008311301",type = "Grinding", tsched = 100, tstart = 300, tend = 400, stage = 1}, s1data = "string"},	
	Act5 = #stage1Activity{shell = #activity_shell{id = "E1",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 1}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Act6 = #stage1Activity{shell = #activity_shell{id = "E2",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 1}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Act7 = #stage1Activity{shell = #activity_shell{id = "E3",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 1}, s1data = 5},	
	Act8 = #stage1Activity{shell = #activity_shell{id = "E4",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 1}, s1data = "string"},
	Reply = gen_server:call(PID, {save_sched_activities, "Daniel", "Human", [Sched1, Sched2, Sched3, Act5, Act6, Act7, Act8]}),
	io:format("\nExpecting: success\n"),
	case Reply of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR}->
					io:format("\nResult:"),
					io:format(SR);
				R->
					io:format(R)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {save_sched_activities, "Daniel", "Human", [Sched4]}),
	io:format("\nExpecting: success\n"),
	case Reply2 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR2}->
					io:format("\nResult:"),
					io:format(SR2);
				R2->
					io:format(R2)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {save_sched_activities, "Daniel", "Human", Sched4}),
	io:format("\nExpecting: invalid input arguments\n"),
	case Reply3 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR3}->
					io:format("\nResult:"),
					io:format(SR3);
				R3->
					io:format(R3)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply3)
	end,
	Reply4 = gen_server:call(PID, {save_sched_activities, "Daniel", "Human", [invalid_sched]}),
	io:format("\nExpecting: not valid activities\n"),
	case Reply4 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR4}->
					io:format("\nResult:"),
					io:format(SR4);
				R4->
					io:format(R4)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply4)
	end.
test_save_exe()->
	{ok, PID} = google_sheet_saver:start_link(),
	Exe1 = #stage2Activity{shell = #activity_shell{id = "Act_W_202008311202",type = "Welding", tsched = 5, tstart = 6, tend = 7, stage = 2}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Exe2 = #stage2Activity{shell = #activity_shell{id = "Act_W_202008311302",type = "Welding", tsched = 10, tstart = 30, tend = 40, stage = 2}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Exe3 = #stage2Activity{shell = #activity_shell{id = "Act_G_202008311202",type = "Grinding", tsched = 50, tstart = 60, tend = 70, stage = 2}, s1data = 5},	
	Exe4 = #stage2Activity{shell = #activity_shell{id = "Act_G_202008311302",type = "Grinding", tsched = 100, tstart = 300, tend = 400, stage = 2}, s1data = "string"},
	Act5 = #stage2Activity{shell = #activity_shell{id = "E1",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 2}, s1data = {weldLength, 12, weldTime, 20, weldQuality, 6}},	
	Act6 = #stage2Activity{shell = #activity_shell{id = "E2",type = "Welding", tsched = 999, tstart = 999, tend = 999, stage = 2}, s1data = {weldLength, 12, weldTime, 6, weldQuality, 10}},	
	Act7 = #stage2Activity{shell = #activity_shell{id = "E3",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 2}, s1data = 5},	
	Act8 = #stage2Activity{shell = #activity_shell{id = "E4",type = "Grinding", tsched = 999, tstart = 999, tend = 999, stage = 2}, s1data = "string"},
	Sched5 = #stage1Activity{shell = #activity_shell{id = "Act_G_202008311305",type = "Grinding", tsched = 100, tstart = 300, tend = 400, stage = 2}, s1data = "string"},	
	Reply = gen_server:call(PID, {save_exe_activities, "Daniel", "Human", [Exe1, Exe2, Exe3, Act5, Act6, Act7, Act8]}),
	io:format("\nExpecting: success\n"),
	case Reply of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR}->
					io:format("\nResult:"),
					io:format(SR);
				R->
					io:format(R)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {save_exe_activities, "Daniel", "Human", [Exe4]}),
	io:format("\nExpecting: success\n"),
	case Reply2 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR2}->
					io:format("\nResult:"),
					io:format(SR2);
				R2->
					io:format(R2)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {save_exe_activities, "Daniel", "Human", Exe4}),
	io:format("\nExpecting: invalid input arguments\n"),
	case Reply3 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR3}->
					io:format("\nResult:"),
					io:format(SR3);
				R3->
					io:format(R3)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply3)
	end,
	Reply4 = gen_server:call(PID, {save_exe_activities, "Daniel", "Human", [Sched5]}),
	io:format("\nExpecting: not valid activities\n"),
	case Reply4 of
		{wait_for_reply_from,_}->
			receive
				{save_result, SR4}->
					io:format("\nResult:"),
					io:format(SR4);
				R4->
					io:format(R4)
			end;
		_->
			io:format("\nResult:"),
			io:format(Reply4)
	end.

test_get_atr()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {get_atr_by_ids, "Daniel", "Human", ["WeldingSpeed", "WeldingQuality"]}),
	io:format("\nExpecting WeldingSpeed and WeldingQuality attributes\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {get_atr_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting WeldingSpeed and WeldingQuality attributes\n"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {get_atr_by_contexts, "Daniel", "Human", ["bpm"]}),
	io:format("\nExpecting AvgHeartRate attribute\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	Reply4 = gen_server:call(PID, {get_atr_by_types, "Daniel", "Human", "Welding"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	Reply5 = gen_server:call(PID, {get_atr_by_ids, "Daniel", "Human", "WeldingSpeed"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	Reply6 = gen_server:call(PID, {get_atr_by_contexts, "Daniel", "Human", "bpm"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	Reply7 = gen_server:call(PID, {get_atr_by_ids, "Daniel", "Human", ["NonExistant"]}),
	io:format("\nExpecting: none\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end,
	Reply8 = gen_server:call(PID, {get_all_atr, "Daniel", "Human"}),
	io:format("\nExpecting: all attributes\n"),
	case Reply8 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR8}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR8]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply8)
	end.
test_get_bio()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {get_bio_by_ids, "Daniel", "Human", ["Act_W_202008311200","Act_W_202008311300"]}),
	io:format("\nExpecting 2 activities\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {get_bio_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting 2 activities\n"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {get_bio_by_times, "Daniel", "Human", 0,100,"all","all",0,12}),
	io:format("\nExpecting 1 activity\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	Reply4 = gen_server:call(PID, {get_bio_by_types_and_times, "Daniel", "Human", ["Welding"],"all","all",0,100,0,100}),
	io:format("\nExpecting 2 activities\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	Reply5 = gen_server:call(PID, {get_bio_by_ids, "Daniel", "Human", "id"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	Reply6 = gen_server:call(PID, {get_bio_by_types_and_times, "Daniel", "Human", "Welding","all","all",0,100,0,100}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	Reply7 = gen_server:call(PID, {get_bio_by_ids, "Daniel", "Human", ["NonExistant"]}),
	io:format("\nExpecting: none\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end,
	Reply8 = gen_server:call(PID, {get_all_bio, "Daniel", "Human"}),
	io:format("\nExpecting: all activities\n"),
	case Reply8 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR8}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR8]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply8)
	end.
test_get_sched()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {get_sched_by_ids, "Daniel", "Human", ["Act_W_202008311201","Act_W_202008311301"]}),
	io:format("\nExpecting 2 activities\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {get_sched_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting 2 activities\n"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {get_sched_by_times, "Daniel", "Human", 0,100,"all","all",0,12}),
	io:format("\nExpecting 1 activity\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	Reply4 = gen_server:call(PID, {get_sched_by_types_and_times, "Daniel", "Human", ["Welding"],"all","all",0,100,"all","all"}),
	io:format("\nExpecting 2 activities\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	Reply5 = gen_server:call(PID, {get_sched_by_ids, "Daniel", "Human", "id"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	Reply6 = gen_server:call(PID, {get_sched_by_types_and_times, "Daniel", "Human", "Welding","all","all",0,100,0,100}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	Reply7 = gen_server:call(PID, {get_sched_by_ids, "Daniel", "Human", ["NonExistant"]}),
	io:format("\nExpecting: none\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end,
	Reply8 = gen_server:call(PID, {get_all_sched, "Daniel", "Human"}),
	io:format("\nExpecting: all activities\n"),
	case Reply8 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR8}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR8]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply8)
	end.
test_get_exe()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {get_exe_by_ids, "Daniel", "Human", ["Act_W_202008311202","Act_W_202008311302"]}),
	io:format("\nExpecting 2 activities\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {get_exe_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting 2 activities\n"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {get_exe_by_times, "Daniel", "Human", 0,100,"all","all",0,12}),
	io:format("\nExpecting 1 activity\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	Reply4 = gen_server:call(PID, {get_exe_by_types_and_times, "Daniel", "Human", ["Welding"],"all","all",0,100,"all","all"}),
	io:format("\nExpecting 2 activities\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	Reply5 = gen_server:call(PID, {get_exe_by_ids, "Daniel", "Human", "id"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	Reply6 = gen_server:call(PID, {get_exe_by_types_and_times, "Daniel", "Human", "Welding","all","all",0,100,0,100}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	Reply7 = gen_server:call(PID, {get_exe_by_ids, "Daniel", "Human", ["NonExistant"]}),
	io:format("\nExpecting: none\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end,
	Reply8 = gen_server:call(PID, {get_all_exe, "Daniel", "Human"}),
	io:format("\nExpecting: all activities\n"),
	case Reply8 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR8}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR8]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply8)
	end.	
test_get_act()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {get_act_by_ids, "Daniel", "Human", ["Act_W_202008311202","Act_W_202008311302"]}),
	io:format("\nExpecting 2 stage 2 activities\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	Reply2 = gen_server:call(PID, {get_act_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting two stage 1, two stage 2 and two stage 3 activities\n"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	Reply3 = gen_server:call(PID, {get_act_by_times, "Daniel", "Human", 0,100,"all","all",0,12}),
	io:format("\nExpecting one stage 1, one stage 2 and one stage 3 activity\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	Reply4 = gen_server:call(PID, {get_act_by_types_and_times, "Daniel", "Human", ["Welding"],"all","all",0,100,"all","all"}),
	io:format("\nExpecting 2 stage 1, two stage 2 and two stage 3 activities\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	Reply5 = gen_server:call(PID, {get_act_by_ids, "Daniel", "Human", "id"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	Reply6 = gen_server:call(PID, {get_act_by_types_and_times, "Daniel", "Human", "Welding","all","all",0,100,0,100}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	Reply7 = gen_server:call(PID, {get_act_by_ids, "Daniel", "Human", ["NonExistant"]}),
	io:format("\nExpecting: none\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end,
	Reply8 = gen_server:call(PID, {get_all_act, "Daniel", "Human"}),
	io:format("\nExpecting: all activities\n"),
	case Reply8 of
		{wait_for_reply_from, _}->
			receive
				{get_result, GR8}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR8]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply8)
	end.	
%%%%%%%%%%%%%%%%%%%%%%%%Delete tests%%%%%%%%%%%%%%%%%%%%%%%%%%%
test_delete_atr()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {delete_atr_by_ids, "Daniel", "Human", ["WeldingSpeed", "WeldingQuality"]}),
	io:format("\nCheck google sheets, if deleted weldingspeed and weldingquality\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	timer:sleep(5000),
	Reply2 = gen_server:call(PID, {delete_atr_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting none"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	timer:sleep(5000),
	Reply3 = gen_server:call(PID, {delete_atr_by_contexts, "Daniel", "Human", ["bpm"]}),
	io:format("\nExpecting AvgHeartRate attribute to be deleted in google sheets\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	timer:sleep(5000),
	Reply4 = gen_server:call(PID, {delete_atr_by_types, "Daniel", "Human", "Welding"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	timer:sleep(5000),
	Reply5 = gen_server:call(PID, {delete_atr_by_ids, "Daniel", "Human", "WeldingSpeed"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	timer:sleep(5000),
	Reply6 = gen_server:call(PID, {delete_atr_by_contexts, "Daniel", "Human", "bpm"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	timer:sleep(5000),
	Reply7 = gen_server:call(PID, {delete_all_atr, "Daniel", "Human"}),
	io:format("\nExpecting: all deleted\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end.
test_delete_bio()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {delete_bio_by_ids, "Daniel", "Human", ["Act_W_202008311200","Act_W_202008311300"]}),
	io:format("\nExpecting two activities starting with Act_W deleted\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	timer:sleep(5000),
	Reply2 = gen_server:call(PID, {delete_bio_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting none deleted\n"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	timer:sleep(5000),
	Reply3 = gen_server:call(PID, {delete_bio_by_times, "Daniel", "Human", 0,100,"all","all",0,12}),
	io:format("\nExpecting 1 grinding activity left in bio\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	timer:sleep(5000),
	Reply4 = gen_server:call(PID, {delete_bio_by_types_and_times, "Daniel", "Human", ["Welding"],"all","all",0,100,0,100}),
	io:format("\nExpecting none deleted\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	timer:sleep(5000),
	Reply5 = gen_server:call(PID, {delete_bio_by_ids, "Daniel", "Human", "id"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	timer:sleep(5000),
	Reply6 = gen_server:call(PID, {delete_bio_by_types_and_times, "Daniel", "Human", "Welding","all","all",0,100,0,100}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	timer:sleep(5000),
	Reply7 = gen_server:call(PID, {delete_bio_by_ids, "Daniel", "Human", ["NonExistant"]}),
	io:format("\nExpecting: none\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end,
	timer:sleep(5000),
	Reply8 = gen_server:call(PID, {delete_all_bio, "Daniel", "Human"}),
	io:format("\nExpecting: all activities deleted\n"),
	case Reply8 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR8}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR8]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply8)
	end.
test_delete_sched()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {delete_sched_by_ids, "Daniel", "Human", ["Act_W_202008311201","Act_W_202008311301"]}),
	io:format("\nExpecting 2 activities starting with Act_W deleted\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	timer:sleep(5000),
	Reply2 = gen_server:call(PID, {delete_sched_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting none deleted\n"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	timer:sleep(5000),
	Reply3 = gen_server:call(PID, {delete_sched_by_times, "Daniel", "Human", 0,100,"all","all",0,12}),
	io:format("\nExpecting 1 grinding activity remaining\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	timer:sleep(5000),
	Reply4 = gen_server:call(PID, {delete_sched_by_types_and_times, "Daniel", "Human", ["Welding"],"all","all",0,100,"all","all"}),
	io:format("\nExpecting none deleted\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	timer:sleep(5000),
	Reply5 = gen_server:call(PID, {delete_sched_by_ids, "Daniel", "Human", "id"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	timer:sleep(5000),
	Reply6 = gen_server:call(PID, {delete_sched_by_types_and_times, "Daniel", "Human", "Welding","all","all",0,100,0,100}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	timer:sleep(5000),
	Reply7 = gen_server:call(PID, {delete_sched_by_ids, "Daniel", "Human", ["NonExistant"]}),
	io:format("\nExpecting: none deleted\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end,
	timer:sleep(5000),
	Reply8 = gen_server:call(PID, {delete_all_sched, "Daniel", "Human"}),
	io:format("\nExpecting: all activities deleted\n"),
	case Reply8 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR8}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR8]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply8)
	end.
test_delete_exe()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {delete_exe_by_ids, "Daniel", "Human", ["Act_W_202008311202","Act_W_202008311302"]}),
	io:format("\nExpecting 2 activities starting with Act_W deleted\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	timer:sleep(5000),
	Reply2 = gen_server:call(PID, {delete_exe_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting none deleted\n"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	timer:sleep(5000),
	Reply3 = gen_server:call(PID, {delete_exe_by_times, "Daniel", "Human", 0,100,"all","all",0,12}),
	io:format("\nExpecting 1 grinding activity remaining\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	timer:sleep(5000),
	Reply4 = gen_server:call(PID, {delete_exe_by_types_and_times, "Daniel", "Human", ["Welding"],"all","all",0,100,"all","all"}),
	io:format("\nExpecting none deleted\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	timer:sleep(5000),
	Reply5 = gen_server:call(PID, {delete_exe_by_ids, "Daniel", "Human", "id"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	timer:sleep(5000),
	Reply6 = gen_server:call(PID, {delete_exe_by_types_and_times, "Daniel", "Human", "Welding","all","all",0,100,0,100}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	timer:sleep(5000),
	Reply7 = gen_server:call(PID, {delete_exe_by_ids, "Daniel", "Human", ["NonExistant"]}),
	io:format("\nExpecting: none deleted\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end,
	timer:sleep(5000),
	Reply8 = gen_server:call(PID, {delete_all_exe, "Daniel", "Human"}),
	io:format("\nExpecting: all activities deleted\n"),
	case Reply8 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR8}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR8]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply8)
	end.	
test_delete_act()->
	{ok, PID} = google_sheet_storage:start_link(),
	Reply = gen_server:call(PID, {delete_act_by_ids, "Daniel", "Human", ["Act_W_202008311202","Act_W_202008311302"]}),
	io:format("\nExpecting 2 exe acts deleted\n"),
	case Reply of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply)
	end,
	timer:sleep(5000),
	Reply2 = gen_server:call(PID, {delete_act_by_types, "Daniel", "Human", ["Welding"]}),
	io:format("\nExpecting two stage 1, two stage 2 and two stage 3 activities deleted\n"),
	case Reply2 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR2}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR2]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply2)
	end,
	timer:sleep(5000),
	Reply3 = gen_server:call(PID, {delete_act_by_times, "Daniel", "Human", 0,100,"all","all",0,12}),
	io:format("\nExpecting one grinding activity remaining in bio, exe and sched\n"),
	case Reply3 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR3}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR3]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply3)
	end, 
	timer:sleep(5000),
	Reply4 = gen_server:call(PID, {delete_act_by_types_and_times, "Daniel", "Human", ["Welding"],"all","all",0,100,"all","all"}),
	io:format("\nExpecting none deleted\n"),
	case Reply4 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR4}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR4]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply4)
	end,
	timer:sleep(5000),
	Reply5 = gen_server:call(PID, {delete_act_by_ids, "Daniel", "Human", "id"}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply5 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR5}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR5]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply5)
	end,
	timer:sleep(5000),
	Reply6 = gen_server:call(PID, {delete_act_by_types_and_times, "Daniel", "Human", "Welding","all","all",0,100,0,100}),
	io:format("\nExpecting: invalid input parameters\n"),
	case Reply6 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR6}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR6]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply6)
	end,
	timer:sleep(5000),
	Reply7 = gen_server:call(PID, {delete_act_by_ids, "Daniel", "Human", ["NonExistant"]}),
	io:format("\nExpecting: none deleted\n"),
	case Reply7 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR7}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR7]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply7)
	end,
	timer:sleep(5000),
	Reply8 = gen_server:call(PID, {delete_all_act, "Daniel", "Human"}),
	io:format("\nExpecting: all activities deleted\n"),
	case Reply8 of
		{wait_for_reply_from, _}->
			receive
				{del_result, GR8}->
					io:format("\nResult:"),
					io:format(lists:flatten(io_lib:format("~p",[GR8]))),
					io:format("\n");
				_->
					io:format("unexpected")
			end;
		_->
			io:format("Result:"),
			io:format(Reply8)
	end.	