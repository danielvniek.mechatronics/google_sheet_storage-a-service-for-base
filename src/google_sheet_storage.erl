%% @author Daniel

-module(google_sheet_storage).
-behaviour(gen_server).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-include("base_records.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([start_link/0, save/8, get_del_or_pop/8, doha_registration_loop/2]).
-record(serv_descr,{type,name}).
%% ====================================================================
%% Behavioural functions
%% ====================================================================
-record(google_sheets_state, {python_tcp_adr = {127,0,0,1},python_tcp_port = 11000,active_requests = #{}}).

start_link() ->
   {ok,_PID} = gen_server:start_link(?MODULE, [], []).


init([]) ->
   PID = self(),
   Address = #address{type = pid, value = PID},
   SD1 = #serv_descr{type = "base_biography_storage",details = #######TODO},
   SD2 = #serv_descr{type = "base_attributes_storage",details = TODO},
   SD3 = #serv_descr{type = "base_schedule_storage",details = TODO},
   SD4 = #serv_descr{type = "base_execution_storage", details = TODO},
   Services = [SD1, SD2, SD3, SD4],
   Identity = #{id => "Google_Sheets_Storage",local_name => "Google_Sheets_Storage"},
   AtomName = google_sheets_storage,
   MyBC = #{identity => Identity, type = "Storage",address => #{ipv4 =>none,erl_adr = AtomName}, services => Services, protocols => erl,parent => pending},
   %spawn(google_sheet_storage,doha_registration_loop,[MyBC,0]),%%%%%%%%%%%%%%%%%%%to be uncommented once doha is finalized
   {ok, #google_sheets_state{active_requests = #{}}}.

%% handle_calls for SAVING
handle_call({done_with_request, ID},From, State)->
	{Pid,_Ref} = From,
	{Present,Value}=maps:find(ID, State#google_sheets_state.active_requests),
	case Present of
		ok->
			case Value of
				Pid->
					ActiveReqs = maps:remove(ID, State#google_sheets_state.active_requests),
					NewState = State#google_sheets_state{active_requests = ActiveReqs};
				_->
					NewState = State
			end;
		_->
			NewState = State
	end,
	{noreply, NewState};
	
	
handle_call({save_attributes, ID, HolonType, Attributes}, From, State)->
	%check if ID and HolonType are strings and if Attributes is a LIST with valid attributes
	case io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Attributes) of
		true-> case checkIfAttributes(Attributes) of 
		true->
			case maps:is_key(ID, State#google_sheets_state.active_requests) of
				true->
					Reply = {busy, "Please try again in a small while"},
					{reply, Reply, State};
				_->
					{ok,PID}=spawn(google_sheet_storage,save, [save,atr,ID, HolonType,Attributes, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From]), 
					ActiveReqs = maps:put(ID, PID, State#google_sheets_state.active_requests),
					NewState = State#google_sheets_state{active_requests = ActiveReqs},
					{noreply, NewState}
			end;
		_->
			{reply,{error,"Attributes list does not contain valid attributes. Please note that in a deprecated version of BASE atoms were allowed data types for id,type and context but in this version only strings are allowed."},State}
		end;
		_->
			{reply,{error,"Invalid input arguments"},State}
	end;
handle_call({save_bio_activities, ID, HolonType, Activities}, From, State)->
	case io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Activities) of
		true-> case checkIfBio(Activities) of 
		true->
			case maps:is_key(ID, State#google_sheets_state.active_requests) of
				true->
					Reply = {busy, "Please try again in a small while"},
					{reply, Reply, State};
				_->
					{ok,PID}=spawn(google_sheet_storage,save, [save,bio,ID, HolonType,Activities, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From]), 
					ActiveReqs = maps:put(ID, PID, State#google_sheets_state.active_requests),
					NewState = State#google_sheets_state{active_requests = ActiveReqs},
					{noreply, NewState}
			end;
		_->
			Reply = "Activity list does not contain valid activities. Please note that in a deprecated version of BASE atoms were allowed data types for id and type, but in this version only strings are allowed. Also ensure that you activities' stage is 3",
			{reply, Reply, State}
		end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;	
	
handle_call({save_sched_activities, ID, HolonType, Activities}, From, State)->
	case io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Activities) of
		true-> case checkIfSched(Activities) of 
		true->
			case maps:is_key(ID, State#google_sheets_state.active_requests) of
				true->
					Reply = {busy, "Please try again in a small while"},
					{reply, Reply, State};
				_->
					{ok,PID}=spawn(google_sheet_storage,save, [save,sched,ID, HolonType,Activities, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From]), 
					ActiveReqs = maps:put(ID, PID, State#google_sheets_state.active_requests),
					NewState = State#google_sheets_state{active_requests = ActiveReqs},
					{noreply, NewState}
			end;
		_->
			Reply = "Activity list does not contain valid activities. Please note that in a deprecated version of BASE atoms were allowed data types for id and type, but in this version only strings are allowed. Also ensure that you activities' stage is 1",
			{reply, Reply, State}
		end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;	
	
handle_call({save_exe_activities, ID, HolonType, Activities}, From, State)->
	case io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Activities) of
		true-> case checkIfExe(Activities) of 
		true->
			case maps:is_key(ID, State#google_sheets_state.active_requests) of
				true->
					Reply = {busy, "Please try again in a small while"},
					{reply, Reply, State};
				_->
					{ok,PID}=spawn(google_sheet_storage,save, [save,exe,ID, HolonType,Activities, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From]), 
					ActiveReqs = maps:put(ID, PID, State#google_sheets_state.active_requests),
					NewState = State#google_sheets_state{active_requests = ActiveReqs},
					{noreply, NewState}
			end;
		_->
			Reply = "Activity list does not contain valid activities. Please note that in a deprecated version of BASE atoms were allowed data types for id and type, but in this version only strings are allowed. Also ensure that you activities' stage is 2",
			{reply, Reply, State}
		end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;	
	
handle_call({update_attributes, ID, HolonType, Attributes}, From, State)->
	%check if ID and HolonType are strings and if Attributes is a LIST with valid attributes
	case io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Attributes) of
		true-> case checkIfAttributes(Attributes) of 
		true->
			case maps:is_key(ID, State#google_sheets_state.active_requests) of
				true->
					Reply = {busy, "Please try again in a small while"},
					{reply, Reply, State};
				_->
					{ok,PID}=spawn(google_sheet_storage,save, [update,atr,ID, HolonType,Attributes, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From]), 
					ActiveReqs = maps:put(ID, PID, State#google_sheets_state.active_requests),
					NewState = State#google_sheets_state{active_requests = ActiveReqs},
					{noreply, NewState}
			end;
		_->
			{reply,{error,"Attributes list does not contain valid attributes. Please note that in a deprecated version of BASE atoms were allowed data types for id,type and context but in this version only strings are allowed."},State}
		end;
		_->
			{reply,{error,"Invalid input arguments"},State}
	end;
handle_call({update_bio_activities, ID, HolonType, Activities}, From, State)->
	case io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Activities) of
		true-> case checkIfBio(Activities) of 
		true->
			case maps:is_key(ID, State#google_sheets_state.active_requests) of
				true->
					Reply = {busy, "Please try again in a small while"},
					{reply, Reply, State};
				_->
					{ok,PID}=spawn(google_sheet_storage,save, [update,bio,ID, HolonType,Activities, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From]), 
					ActiveReqs = maps:put(ID, PID, State#google_sheets_state.active_requests),
					NewState = State#google_sheets_state{active_requests = ActiveReqs},
					{noreply, NewState}
			end;
		_->
			Reply = "Activity list does not contain valid activities. Please note that in a deprecated version of BASE atoms were allowed data types for id and type, but in this version only strings are allowed. Also ensure that you activities' stage is 3",
			{reply, Reply, State}
		end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;	
	
handle_call({update_sched_activities, ID, HolonType, Activities}, From, State)->
	case io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Activities) of
		true-> case checkIfSched(Activities) of 
		true->
			case maps:is_key(ID, State#google_sheets_state.active_requests) of
				true->
					Reply = {busy, "Please try again in a small while"},
					{reply, Reply, State};
				_->
					{ok,PID}=spawn(google_sheet_storage,save, [update,sched,ID, HolonType,Activities, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From]), 
					ActiveReqs = maps:put(ID, PID, State#google_sheets_state.active_requests),
					NewState = State#google_sheets_state{active_requests = ActiveReqs},
					{noreply, NewState}
			end;
		_->
			Reply = "Activity list does not contain valid activities. Please note that in a deprecated version of BASE atoms were allowed data types for id and type, but in this version only strings are allowed. Also ensure that you activities' stage is 1",
			{reply, Reply, State}
		end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;	
	
handle_call({update_exe_activities, ID, HolonType, Activities}, From, State)->
	case io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Activities) of
		true-> case checkIfExe(Activities) of 
		true->
			case maps:is_key(ID, State#google_sheets_state.active_requests) of
				true->
					Reply = {busy, "Please try again in a small while"},
					{reply, Reply, State};
				_->
					{ok,PID}=spawn(google_sheet_storage,save, [update,exe,ID, HolonType,Activities, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From]), 
					ActiveReqs = maps:put(ID, PID, State#google_sheets_state.active_requests),
					NewState = State#google_sheets_state{active_requests = ActiveReqs},
					{noreply, NewState}
			end;
		_->
			Reply = "Activity list does not contain valid activities. Please note that in a deprecated version of BASE atoms were allowed data types for id and type, but in this version only strings are allowed. Also ensure that you activities' stage is 2",
			{reply, Reply, State}
		end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
%%handle_calls for GETTING
%%handle_calls for getting atr

handle_call({get_del_or_pop_all_atr, ID, HolonType,GetDelOrPop}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			FilterMap = #{"SearchBy" => "type", "SearchFor" => ["all"]},
			Args = ["atr",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
			NewState = get_if_inactive(Args, ID, From, State),
			{noreply, NewState};
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_atr_by_ids, ID, HolonType, GetDelOrPop,IDs}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(IDs) and not(io_lib:printable_list(IDs))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(IDs) of
				true->
					FilterMap = #{"SearchBy" => "id", "SearchFor" => IDs},
					Args = ["atr",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of IDs list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_atr_by_types, ID, HolonType, GetDelOrPop,Types}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Types) and not(io_lib:printable_list(Types))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Types) of
				true->
					FilterMap = #{"SearchBy" => "type", "SearchFor" => Types},
					Args = ["atr",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
				_->
					Reply = "The elements of Types list must be of type string",
					{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_atr_by_contexts, ID, HolonType, GetDelOrPop,Contexts}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Contexts) and not(io_lib:printable_list(Contexts))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Contexts) of
				true->
					FilterMap = #{"SearchBy" => "context", "SearchFor" => Contexts},
					Args = ["atr",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
				_->
					Reply = "The elements of Contexts list must be of type string",
					{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
%%handle_calls for getting bio
handle_call({get_del_or_pop_all_bio, ID, HolonType,GetDelOrPop}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType)and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			FilterMap = #{"SearchBy" => "times_and_types", "type" => "all", "tsched"=>"all", "tstart"=>"all", "tend"=>"all"},
			Args = ["bio",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
			NewState = get_if_inactive(Args, ID, From, State),
			{noreply, NewState};
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_bio_by_ids, ID, HolonType, GetDelOrPop,IDs}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(IDs) and not(io_lib:printable_list(IDs))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(IDs) of
				true->
					FilterMap = #{"SearchBy" => "id", "SearchFor" => IDs},
					Args = ["bio",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of IDs list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_bio_by_types, ID, HolonType, GetDelOrPop,Types}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Types) and not(io_lib:printable_list(Types))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Types) of
				true->
					FilterMap = #{"SearchBy" => "times_and_types", "type" => Types, "tsched"=>"all", "tstart"=>"all", "tend"=>"all"},
					Args = ["bio",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of Types list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_bio_by_times, ID, HolonType, GetDelOrPop,TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType)and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of%%%dear future developer, do not check if the times are integers, this is done in the createFilterMap and if they are not ints "all" is used
		true->
			FilterMap = createFilterMap("all", TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow,TendHigh),
			Args = ["bio",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
			NewState = get_if_inactive(Args, ID, From, State),
			{noreply, NewState};
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_bio_by_types_and_times, ID, HolonType, GetDelOrPop,Types, TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Types) and not(io_lib:printable_list(Types))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Types) of
				true->
					FilterMap = createFilterMap(Types, TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow,TendHigh),
					Args = ["bio",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of Types list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
%%handle_calls for getting execution
handle_call({get_del_or_pop_all_exe, ID, HolonType,GetDelOrPop}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType)and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			FilterMap = #{"SearchBy" => "times_and_types", "type" => "all", "tsched"=>"all", "tstart"=>"all", "tend"=>"all"},
			Args = ["exe",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
			NewState = get_if_inactive(Args, ID, From, State),
			{noreply, NewState};
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_exe_by_ids, ID, HolonType, GetDelOrPop,IDs}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(IDs) and not(io_lib:printable_list(IDs))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(IDs) of
				true->
					FilterMap = #{"SearchBy" => "id", "SearchFor" => IDs},
					Args = ["exe",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of IDs list must be of type string",
				{reply, Reply, State}				
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_exe_by_types, ID, HolonType, GetDelOrPop,Types}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Types) and not(io_lib:printable_list(Types))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Types) of
				true->
					FilterMap = #{"SearchBy" => "times_and_types", "type" => Types, "tsched"=>"all", "tstart"=>"all", "tend"=>"all"},
					Args = ["exe",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of Types list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_exe_by_times, ID, HolonType, GetDelOrPop,TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType)and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of%%%dear future developer, do not check if the times are integers, this is done in the createFilterMap and if they are not ints "all" is used
		true->
			FilterMap = createFilterMap("all", TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow,TendHigh),
			Args = ["exe",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
			NewState = get_if_inactive(Args, ID, From, State),
			{noreply, NewState};
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_exe_by_types_and_times, ID, HolonType, GetDelOrPop,Types, TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Types) and not(io_lib:printable_list(Types))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Types) of
				true->
					FilterMap = createFilterMap(Types, TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow,TendHigh),
					Args = ["exe",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of Types list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
%%handle_calls for getting sched
handle_call({get_del_or_pop_all_sched, ID, HolonType,GetDelOrPop}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType)and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			FilterMap = #{"SearchBy" => "times_and_types", "type" => "all", "tsched"=>"all", "tstart"=>"all", "tend"=>"all"},
			Args = ["sched",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
			NewState = get_if_inactive(Args, ID, From, State),
			{noreply, NewState};
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_sched_by_ids, ID, HolonType, GetDelOrPop,IDs}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(IDs) and not(io_lib:printable_list(IDs))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(IDs) of
				true->
					FilterMap = #{"SearchBy" => "id", "SearchFor" => IDs},
					Args = ["sched",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of IDs list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_sched_by_types, ID, HolonType, GetDelOrPop,Types}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Types) and not(io_lib:printable_list(Types))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Types) of
				true->
					FilterMap = #{"SearchBy" => "times_and_types", "type" => Types, "tsched"=>"all", "tstart"=>"all", "tend"=>"all"},
					Args = ["sched",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of Types list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_sched_by_times, ID, HolonType, GetDelOrPop,TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType)and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of%%%dear future developer, do not check if the times are integers, this is done in the createFilterMap and if they are not ints "all" is used
		true->
			FilterMap = createFilterMap("all", TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow,TendHigh),
			Args = ["sched",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
			NewState = get_if_inactive(Args, ID, From, State),
			{noreply, NewState};
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_sched_by_types_and_times, ID, HolonType, GetDelOrPop,Types, TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Types) and not(io_lib:printable_list(Types))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Types) of
				true->
					FilterMap = createFilterMap(Types, TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow,TendHigh),
					Args = ["sched",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of Types list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
%%handle_calls for getting act from any of the bio, exe or sched, but if an act exists in more than one place the highest stage will be given and the others deleted
handle_call({get_del_or_pop_all_act, ID, HolonType,GetDelOrPop}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType)and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			FilterMap = #{"SearchBy" => "times_and_types", "type" => "all", "tsched"=>"all", "tstart"=>"all", "tend"=>"all"},
			Args = ["act",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
			NewState = get_if_inactive(Args, ID, From, State),
			{noreply, NewState};
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_act_by_ids, ID, HolonType, GetDelOrPop,IDs}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(IDs) and not(io_lib:printable_list(IDs))) of
		true->
			case isListOfStrings(IDs) of
				true->
					FilterMap = #{"SearchBy" => "id", "SearchFor" => IDs},
					Args = ["act",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of IDs list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_act_by_types, ID, HolonType, GetDelOrPop,Types}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Types) and not(io_lib:printable_list(Types))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Types) of
				true->
					FilterMap = #{"SearchBy" => "times_and_types", "type" => Types, "tsched"=>"all", "tstart"=>"all", "tend"=>"all"},
					Args = ["act",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of Types list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_act_by_times, ID, HolonType, GetDelOrPop,TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType)and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of%%%dear future developer, do not check if the times are integers, this is done in the createFilterMap and if they are not ints "all" is used
		true->
			FilterMap = createFilterMap("all", TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow,TendHigh),
			Args = ["act",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
			NewState = get_if_inactive(Args, ID, From, State),
			{noreply, NewState};
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;
handle_call({get_del_or_pop_act_by_types_and_times, ID, HolonType, GetDelOrPop,Types, TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh}, From, State)->
	case (io_lib:printable_list(ID) and io_lib:printable_list(HolonType) and erlang:is_list(Types) and not(io_lib:printable_list(Types))and (string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop,"del") or string:equal(GetDelOrPop,"pop"))) of
		true->
			case isListOfStrings(Types) of
				true->
					FilterMap = createFilterMap(Types, TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow,TendHigh),
					Args = ["act",GetDelOrPop,ID, HolonType,FilterMap, State#google_sheets_state.python_tcp_adr,State#google_sheets_state.python_tcp_port, From],
					NewState = get_if_inactive(Args, ID, From, State),
					{noreply, NewState};
			_->
				Reply = "The elements of Types list must be of type string",
				{reply, Reply, State}
			end;
		_->
			Reply = "Invalid input arguments",
			{reply, Reply, State}
	end;

handle_call(_Request, _From, State) ->
    Reply = unknown,
    {reply, Reply, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
%% ====================================================================
%% Internal functions
%% ====================================================================
get_if_inactive(Args, ID, From, State)->
	case maps:is_key(ID, State#google_sheets_state.active_requests) of				
		true->
			Reply = {busy, "Please try again in a small while"},
			gen_server:reply(From, Reply),
			State;
		_->					
			PID = {ok,PID}=spawn(google_sheet_storage,get_del_or_pop, Args), 
			ActiveReqs = maps:put(ID, PID, State#google_sheets_state.active_requests),
			NewState = State#google_sheets_state{active_requests = ActiveReqs}
	end.
createFilterMap(Types, TschedLow, TschedHigh, TstartLow, TstartHigh, TendLow, TendHigh)->
	case erlang:is_integer(TschedLow) and erlang:is_integer(TschedHigh) of
		true->
			Tsched = [TschedLow, TschedHigh];
		_->
			Tsched = "all"
	end,
	case erlang:is_integer(TstartLow) and erlang:is_integer(TstartHigh) of
		true->
			Tstart = [TstartLow, TstartHigh];
		_->
			Tstart = "all"
	end,	
	case erlang:is_integer(TendLow) and erlang:is_integer(TendHigh) of
		true->
			Tend = [TendLow, TendHigh];
		_->
			Tend = "all"
	end,
	FilterMap = #{"SearchBy" => "times_and_types", "type" => Types, "tsched"=>Tsched, "tstart"=>Tstart, "tend"=>Tend},
	FilterMap.

get_del_or_pop(AtrBioExeOrSched,GetDelOrPop,ID, HolonType,FilterMap,Address,Port, From)->
	{Result, Sock} = gen_tcp:connect(Address,Port, 
                                 [{active, true},binary]),
	case Result of
		ok->
			Temp = string:concat(GetDelOrPop, "_"),
			Temp2 = string:concat(Temp,AtrBioExeOrSched),
			OutM = string:concat(Temp2, "!"),
			ok = gen_tcp:send(Sock, OutM),		
			Reply = advanced_loop(Sock,""),
			case string:equal(Reply, "PythonReady") of
				true->
					Message = get_functions:build_get_message(FilterMap, ID, HolonType),
					ok = gen_tcp:send(Sock, Message),
					GetReply = advanced_loop(Sock, ""),
					case string:equal(GetDelOrPop, "get") or string:equal(GetDelOrPop, "pop") of 
						true->
							case string:find(GetReply, "{") of
								nomatch->
									GetResult = {error,GetReply};%%do not change this to an errormessage. The reply can be "none" or "could not open sheet" etc.
								_->
									case AtrBioExeOrSched of
										atr->
											GetResult = {ok,get_functions:jsonToAtrs(GetReply)};
										_->
											GetResult = {ok,get_functions:jsonToActs(GetReply)}
									end
							end;	
						_->
							case string:equal(GetReply, "success") or string:equal(GetReply, "none") of
								true->
									GetResult = {ok,GetReply};
								_->
									GetResult = {error,"Python module had problem deleting in Google Sheet"}
							end
					end;
				_->
					io:format("Python not ready\n"),
					GetResult = {error,Reply}
			end;
		_->
			io:format(Result),
			GetResult = {error,"Could not connect to python module via tcp connection"}
	end,
	gen_server:reply(From,GetResult),
	gen_server:call(google_sheets_storage, {done_with_request, ID}),
	ok.

save(SaveOrUpdate,AtrBioExeOrSched,ID, HolonType,AtrsOrActs,Address,Port, From)->
	{Result, Sock} = gen_tcp:connect(Address,Port, 
                                 [{active, true},binary]),
	case Result of
		ok->
			io:format("Saving\n"),
			case AtrBioExeOrSched of
				atr->
					ok = gen_tcp:send(Sock, "request_atr_save!");
				bio->
					case SaveOrUpdate of
						save->
							ok = gen_tcp:send(Sock, "request_bio_save!");
						_->
							ok = gen_tcp:send(Sock, "request_bio_update!")
					end;
				exe->
					case SaveOrUpdate of
						save->
							ok = gen_tcp:send(Sock, "request_exe_save!");
						_->
							ok = gen_tcp:send(Sock, "request_exe_update!")
					end;
				sched->
					case SaveOrUpdate of
						save->
							ok = gen_tcp:send(Sock, "request_sched_save!");
						_->
							ok = gen_tcp:send(Sock, "request_sched_update!")
					end;
				_->
					io:format("AtrBioExeOrSched not of expected value")
			end,		
			Reply = advanced_loop(Sock, ""),
			case string:equal(Reply, "PythonReady") of
				true->
					case is_list(AtrsOrActs) of
						true->
							Message = save_functions:build_save_message(AtrBioExeOrSched,"",AtrsOrActs, ID, HolonType);
						_->
							Message = save_functions:build_save_message(AtrBioExeOrSched,"",[AtrsOrActs], ID, HolonType)
					end,
					io:format(Message),
					ok = gen_tcp:send(Sock, Message),
					SaveReply = advanced_loop(Sock,""),
					case string:equal(SaveReply, "success") of
						true->
							SaveResult = {ok,"success"};
						_->
							SaveResult = {error,"Python module had problem saving to Google Sheet"}
					end;
				_->
					io:format("Python not ready\n"),
					SaveResult = {error,Reply}
			end;
		_->
			io:format(Result),
			SaveResult = {error,"Could not connect to python module via tcp connection"}
	end,
	gen_server:reply(From, SaveResult),
	gen_server:call(google_sheets_storage, {done_with_request, ID}),
	ok.
advanced_loop(S, BigData)->
	receive
		{tcp, S,Data}->
			BigDataNew = string:concat(BigData, erlang:binary_to_list(Data)),
			case lists:last(BigDataNew) of
				33->
					ExcRem = string:trim(BigDataNew, trailing, "!"),
					ExcRem;
				_->
					advanced_loop(S, BigDataNew)
			end;
		{tcp_closed,S}->
			io:format("\nSocket closed\n"),
			ok
	end.
%%%%%%%%%%%%%%%%%%%%%%CHECK FUNCTIONS USED BY CALLS TO VERIFY INPUT ARGUMENTS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
checkIfAttributes([])->
	true;
checkIfAttributes([H|T])->
	case erlang:is_record(H, base_attribute, 5) of
		true->
			case (io_lib:printable_list(H#base_attribute.id) and io_lib:printable_list(H#base_attribute.type) and (io_lib:printable_list(H#base_attribute.context) or erlang:is_atom(H#base_attribute.context))) of
				true->
					Res = true,
					checkIfAttributes(T);
				_->
					Res = false
			end;
		_->
			Res = false
	end,
	Res.

checkIfBio([])->
	true;
checkIfBio([H|T])->
	case erlang:is_record(H, stage3Activity, 5) of
		true->
			case erlang:is_record(H#stage3Activity.shell, activity_shell, 7) of
				true->
					case (io_lib:printable_list(H#stage3Activity.shell#activity_shell.id) and io_lib:printable_list(H#stage3Activity.shell#activity_shell.type) and erlang:is_integer(H#stage3Activity.shell#activity_shell.tsched) and erlang:is_integer(H#stage3Activity.shell#activity_shell.tstart) and erlang:is_integer(H#stage3Activity.shell#activity_shell.tend)) of
						true->
							case H#stage3Activity.shell#activity_shell.stage of
								3->
									Res = true,
									checkIfBio(T);
								_->
									Res = false
							end;
						_->
							Res = false
					end;
				_->
					Res = false
			end;
		_->
			Res = false
	end,
	Res.	

checkIfExe([])->
	true;
checkIfExe([H|T])->
	case erlang:is_record(H, stage2Activity, 4) of
		true->
			case erlang:is_record(H#stage2Activity.shell, activity_shell, 7) of
				true->
					case (io_lib:printable_list(H#stage2Activity.shell#activity_shell.id) and io_lib:printable_list(H#stage2Activity.shell#activity_shell.type) and erlang:is_integer(H#stage2Activity.shell#activity_shell.tsched) and erlang:is_integer(H#stage2Activity.shell#activity_shell.tstart) and erlang:is_integer(H#stage2Activity.shell#activity_shell.tend)) of
						true->
							case H#stage2Activity.shell#activity_shell.stage of
								2->
									Res = true,
									checkIfExe(T);
								_->
									Res = false
							end;
						_->
							Res = false
					end;
				_->
					Res = false
			end;
		_->
			Res = false
	end,
	Res.

checkIfSched([])->
	true;
checkIfSched([H|T])->
	case erlang:is_record(H, stage1Activity, 3) of
		true->
			case erlang:is_record(H#stage1Activity.shell, activity_shell, 7) of
				true->
					case (io_lib:printable_list(H#stage1Activity.shell#activity_shell.id) and io_lib:printable_list(H#stage1Activity.shell#activity_shell.type) and erlang:is_integer(H#stage1Activity.shell#activity_shell.tsched) and erlang:is_integer(H#stage1Activity.shell#activity_shell.tstart) and erlang:is_integer(H#stage1Activity.shell#activity_shell.tend)) of
						true->
							case H#stage1Activity.shell#activity_shell.stage of
								1->
									Res = true,
									checkIfSched(T);
								_->
									Res = false
							end;
						_->
							Res = false
					end;
				_->
					Res = false
			end;
		_->
			Res = false
	end,
	Res.

isListOfStrings([])->
	true;
isListOfStrings([H|T])->
	case io_lib:printable_list(H) of
		true->
			Res = true,
			isListOfStrings(T);
		_->
			Res = false
	end,
	Res.
	
doha_registration_loop(MyBC, Retries)->
  true = erlang:set_cookie(shelly@localhost,base),
  timer:sleep(1000),
  case net_adm:ping(doha@localhost) of
    pong ->
      io:format("~n DOHA online... ~n"),
      Result = gen_server:call({global, department_of_holon_affairs}, {register_holon, MyBC}),
	  io:format(Result);
    pang ->
      io:format("~n Connection with DOHA failed, retrying in 2s... ~n"),
      if
        Retries > 4 ->
          io:format("~n The department of holon affairs is not responding. Try organising a protest. ~n Retrying in 5 min ~n"),
          spawn(fun()-> timer:sleep(5000 * 60) end),
          doha_registration_loop(MyBC,0);
        true->
          spawn(fun()-> timer:sleep(2000),
            doha_registration_loop(MyBC,Retries+1)
                end)
      end
  end.	
	
	








