-record(activity_shell,{id::term(),type::term(),tsched::number(),tstart::number(),tend::number(),stage:: 1|2|3}).
-record(stage1Activity,
          {shell :: term(),%,activity_shell(),
            s1data :: term()}).

-record(stage2Activity,
          {shell :: term(), %activity_shell(),
            s1data :: term(),
            s2data :: term()}).

-record(stage3Activity,
          {shell :: term(), %TODO make this defined as activity_shell
            s1data :: term(),
            s2data :: term(),
            s3data :: term()}).
-record(base_attribute,
{
  id :: string(),
  type:: personal|contextual,
  context :: term(),
  value:: term()
}).
-type address() ::{atom(),atom(),term()}.
-record(address,
{
  type:: atom(), %The type of the address can be pid|ip|url
  value:: term() %The actual value which will be a PID or an IP and port
}).

-type service_desc() ::{string(),term()}.
-record(service_desc,
{
  service_name::string(),
  service_deets::term()
}).