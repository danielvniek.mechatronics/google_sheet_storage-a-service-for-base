import gspread
from oauth2client.service_account import ServiceAccountCredentials
import threading
import socket
import json
from gspread_formatting import *
import tkinter as tk
import time
import trace
import sys
import datetime
import pandas as pd
import openpyxl

class thread_with_trace(threading.Thread): ##this class is created so a request handling thread can be killed if it is still alive after 60s
  def __init__(self, *args, **keywords): 
    threading.Thread.__init__(self, *args, **keywords) 
    self.killed = False
  
  def start(self): 
    self.__run_backup = self.run 
    self.run = self.__run       
    threading.Thread.start(self) 
  def get_ident(self):
      return self.ident
  def __run(self): 
    sys.settrace(self.globaltrace) 
    self.__run_backup() 
    self.run = self.__run_backup 
  
  def globaltrace(self, frame, event, arg): 
    if event == 'call': 
      return self.localtrace 
    else: 
      return None

  def localtrace(self, frame, event, arg): 
    if self.killed: 
      if event == 'line': 
        raise SystemExit() 
    return self.localtrace 
  
  def kill(self): 
    print("killing a tread")
    self.killed = True
    
def raise_timeout_exception(T, thread):# if a thread takes too long (>60s) to finish handling a request, it is terminated
    global NrProcesses
    global ErrorM
    time.sleep(T)
    if thread.is_alive():
        print("still alive")
        ErrorM.set("ERROR: A request took too long(>60s) to complete and was terminated")
        if ClashPreventerList != []:
            for c in ClashPreventerList:
                T_ID = c[0]
                if T_ID==thread.get_ident():
                    print("Deleting clash entry before killing thread")
                    ClashPreventerList.remove(c)
        thread.kill()
        NrProcesses.set(NrProcesses.get()-1)
        NrProcessesStr.set("Number of active erlang requests being handled: %d" %NrProcesses.get())

def remove_from_clash_list(JsonMsg):#called by listen_and_connect after finnishing handling a request
    DecodedJson = json.loads(JsonMsg)
    HolonName = DecodedJson["HolonName"]
    HolonType = DecodedJson["HolonType"]
    if ClashPreventerList != []:
        for c in ClashPreventerList:
            HN = c[1]
            HT = c[2]
            if  HN == HolonName and HT == HolonType:
                ClashPreventerList.remove(c)
                print("clean removal")
                break



def convert_to_string_if_array(dataRaw):
    if isinstance(dataRaw, list)==True:
        data = str(dataRaw)
    else:
        data = dataRaw
    return data

def buildJsonAtrs(List, ss):#used by find function for get requests to build the return message to erlang for found attributes
    TotalDict = {}
    for L in List:
        AtrId = strToJsonDataType(L[0])
        AtrType = strToJsonDataType(L[1])
        AtrContext = strToJsonDataType(L[2])
        AtrVal = strToJsonDataType(L[3])
        AtrDict = {'id':AtrId, 'type':AtrType, 'context':AtrContext, 'value':AtrVal}
        TotalDict.update({AtrId:AtrDict})    
    Msg = json.dumps(TotalDict)           
    return Msg

def deleteAct(ss, ActDict):  #the following function is not used for delete act calls, but by python if it finds that there is more than one activity with the same ID so it deletes 'old' activities, for example if an activity was in sched and then put into execution as well without deleting the sched
    Stage = ActDict["stage"]
    if Stage ==1:
        Sheet = ss.worksheet("Schedule")
    elif Stage ==2:
        Sheet = ss.worksheet("Execution")
    else:
        Sheet = ss.worksheet("Biography") 
    cell_list = Sheet.findall(ActDict["id"], in_column=1)
    for CellObj in cell_list:
        L = Sheet.row_values(CellObj.row)
        ActId = strToJsonDataType(L[0])
        ActType = strToJsonDataType(L[1])
        Tsched = strToJsonDataType(L[2])
        Tstart = strToJsonDataType(L[3])
        Tend = strToJsonDataType(L[4])
        Stage = strToJsonDataType(L[5])
        S1data = strToJsonDataType(L[6])
        if len(L)>7:
            S2data = strToJsonDataType(L[7])
            Dict = {'id':ActId, 'type':ActType, 'tsched':Tsched, 'tstart':Tstart, 'tend':Tend, 'stage':Stage, 's1data':S1data, 's2data':S2data}
            if len(L)>8:
                S3data = strToJsonDataType(L[8])
                Dict = {'id':ActId, 'type':ActType, 'tsched':Tsched, 'tstart':Tstart, 'tend':Tend, 'stage':Stage, 's1data':S1data, 's2data':S2data, 's3data':S3data} 
        else:
            Dict = {'id':ActId, 'type':ActType, 'tsched':Tsched, 'tstart':Tstart, 'tend':Tend, 'stage':Stage, 's1data':S1data}
        if Dict==ActDict:
            Sheet.delete_rows(CellObj.row)
            break

    
def buildJsonActs(List,ss):#used by find function for get requests to build the return message to erlang for found activities
    TotalDict = {}
    for L in List:
        ActId = strToJsonDataType(L[0])
        ActType = strToJsonDataType(L[1])
        Tsched = strToJsonDataType(L[2])
        Tstart = strToJsonDataType(L[3])
        Tend = strToJsonDataType(L[4])
        Stage = strToJsonDataType(L[5])
        S1data = strToJsonDataType(L[6])
        if len(L)>7:
            S2data = strToJsonDataType(L[7])
            ActDict = {'id':ActId, 'type':ActType, 'tsched':Tsched, 'tstart':Tstart, 'tend':Tend, 'stage':Stage, 's1data':S1data, 's2data':S2data}
            if len(L)>8:
                S3data = strToJsonDataType(L[8])
                ActDict = {'id':ActId, 'type':ActType, 'tsched':Tsched, 'tstart':Tstart, 'tend':Tend, 'stage':Stage, 's1data':S1data, 's2data':S2data, 's3data':S3data} 
        else:
            ActDict = {'id':ActId, 'type':ActType, 'tsched':Tsched, 'tstart':Tstart, 'tend':Tend, 'stage':Stage, 's1data':S1data}
        ExistingId = TotalDict.get(ActId)
        #the following code ensures that if another act with same id exists that the one with the highest stage, or earliest tsched or latest tstart or tend is kept and the other deleted
        if ExistingId == None:
            TotalDict.update({ActId:ActDict})    
        else:
            ExStage = ExistingId.get("stage")
            ExTsched = ExistingId.get("tsched")
            ExTstart = ExistingId.get("tstart")
            ExTend = ExistingId.get("tend")
            if ExStage>Stage:
                deleteAct(ss,ActDict)
            elif ExStage<Stage:
                TotalDict.update({ActId:ActDict}) 
                deleteAct(ss, ExistingId)
            elif Stage == 1 and Tsched<ExTsched:
                    TotalDict.update({ActId:ActDict})
                    deleteAct(ss, ExistingId)
            elif Stage ==2 and Tstart>ExTstart:
                     TotalDict.update({ActId:ActDict})
                     deleteAct(ss, ExistingId)
            elif Stage ==3 and Tend>ExTend:
                     TotalDict.update({ActId:ActDict})
                     deleteAct(ss, ExistingId)
            else:
                deleteAct(ss,ActDict)
    Msg = json.dumps(TotalDict)           
    return Msg
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
def strToJsonDataType(RawData):
        if RawData[0]=='[':
            ArrContent = RawData[1:len(RawData)-1]
            Return = list(ArrContent.split(","))
            count = 0
            while count<len(Return):
                Return[count] = int(Return[count])
                count=count+1
        elif RawData.isnumeric():#if int
            Return = int(RawData)
        elif is_number(RawData):#if float
            Return = float(RawData)
        else:
            Return = RawData
        return Return
    
def getNrAtrsOrActs(ActOrAtr, Content):#for logging purposes
    if ActOrAtr == "Activities":
        DecodedJson = json.loads(Content)
        Acts = DecodedJson["Activities"]
        L = len(Acts)
    elif ActOrAtr == "Attributes":
        DecodedJson = json.loads(Content)
        Acts = DecodedJson["Attributes"]
        L = len(Acts) 
    else:
        L = 0
    return L


####Main functions called by each thread handling a request from erlang############
def find(AtrBioExeOrSched,ss,Filter, GetOrDel):#used for the get and delete calls to find the requested acts and delete or return them. The Filter contains the search filters
    print("Finding")
    ##############GET CORRECT SHEET
    Sheets = []
    if AtrBioExeOrSched == "Sched":
        Sheets = [ss.worksheet("Schedule")]
    elif AtrBioExeOrSched =="Exe":
        Sheets = [ss.worksheet("Execution")]
    elif AtrBioExeOrSched == "Bio":
        Sheets = [ss.worksheet("Biography")]
    elif AtrBioExeOrSched =="Atr":
        Sheets = [ss.worksheet("Attributes")]
    elif AtrBioExeOrSched =="Act":
        Sheets = [ss.worksheet("Schedule"),ss.worksheet("Execution"),ss.worksheet("Biography")]
    else:
        print("Invalid AtrBioExeOrSched")
    
    FoundList = []
    curSheetCount = -1
    somethingDeleted = False
    for Sheet in Sheets:
        nrDeleted = 0
        curSheetCount = curSheetCount+1
         ##############GET SEARCH PARAMETERS
        SearchDict = json.loads(Filter)
        SearchBy = SearchDict["SearchBy"]
        SearchFor = SearchDict["SearchFor"]
         ##############Start SEARCH
        RowCount = Sheet.row_count
        if RowCount>1:#if activities exist            
            #############Search by id
            if SearchBy =="id" or SearchBy == "type" or SearchBy == "context":
                if SearchBy == "id":
                    List = Sheet.col_values(1)
                elif SearchBy == "type":
                    List = Sheet.col_values(2)
                elif SearchBy == "context":
                    List = Sheet.col_values(3)
                else:
                    List = []
                    print("Invalid searchType")
                for Element in SearchFor:
                    count = 1
                    while count<RowCount:
                        if List[count] == Element or Element=="all":#do not break if found cause same duplicates of acts can exist and will be deleted in next steps
                            if GetOrDel == "del":
                                Sheet.delete_rows(count+1-nrDeleted)
                                nrDeleted = nrDeleted+1
                                somethingDeleted = True
                            elif GetOrDel == "get":
                                FoundList.append(Sheet.row_values(count+1))
                            elif GetOrDel == "pop":
                                FoundList.append(Sheet.row_values(count+1))
                                Sheet.delete_rows(count+1-nrDeleted)
                                nrDeleted = nrDeleted+1
                                somethingDeleted = True
                            else:
                                print("Invalid request")
                        count = count+1
            #############Search by times and types
            if SearchBy =="times_and_types":
                ##get time and type parameters
                Types = SearchFor["types"]
                Tsched = SearchFor["tsched"]
                Tstart = SearchFor["tstart"]
                Tend = SearchFor["tend"]
                ##
                SearchList = []
                CompareList = []
                if isinstance(Types, str):
                    Types = [Types]
                if Types[0]!= "all" and len(Types)>0:
                    TypeList = Sheet.col_values(2)
                if (Tsched!="all"):
                    SearchList.append(Sheet.col_values(3))#add tsched column
                    CompareList.append(Tsched)
                if (Tstart!="all"):
                    SearchList.append(Sheet.col_values(4))#add tstart column
                    CompareList.append(Tstart)
                if (Tend!="all"):
                    SearchList.append(Sheet.col_values(5))#add tend column
                    CompareList.append(Tend)
                count=1
                ##########start time and type search process
                while count<RowCount:
                    for T in Types:#check through Types list through all types specified by requester. 
                        if T=="all" or TypeList[count]==T:
                            Found = True
                            newListCount = 0
                            while newListCount < len(CompareList):#check through the rest of the time parameters if they were specified
                                NewList = SearchList[newListCount]
                                NewCompare = CompareList[newListCount]                              
                                if (int(NewList[count])>=NewCompare[0] and int(NewList[count])<=NewCompare[1]):#NewCompare contains the time interval with NewCompare[0] having the lower limit and NewCompare[1] the upper limit
                                    newListCount = newListCount+1
                                    Found = True
                                else:
                                    Found = False
                                    break
                            if Found==True:
                                if GetOrDel == "del":
                                    Sheet.delete_rows(count+1-nrDeleted)
                                    nrDeleted = nrDeleted + 1
                                    somethingDeleted = True
                                elif GetOrDel == "get":
                                    FoundList.append(Sheet.row_values(count+1))
                                elif GetOrDel == "pop":
                                    FoundList.append(Sheet.row_values(count+1))
                                    Sheet.delete_rows(count+1-nrDeleted)
                                    nrDeleted = nrDeleted + 1
                                    somethingDeleted = True
                                else:
                                    print("Invalid request")
                            break#Once a type is found to be equal the rest of the Type list is not checked for this sheet entry
                    count = count+1
    if len(FoundList)>0 and (GetOrDel == "get" or GetOrDel == "pop"):
        if AtrBioExeOrSched == "Atr":
            ReplyMsg = buildJsonAtrs(FoundList, ss)
        else:
            ReplyMsg = buildJsonActs(FoundList, ss)
    elif somethingDeleted==True and GetOrDel =="del":
        ReplyMsg = "success"
    else:
        ReplyMsg = "none"              
    ##change this to receive json with tsched tstart and tend all in with their respective relativities
    return ReplyMsg 

def update_or_create(Sheet, IdList, ValList, OtherActSheets):     #called by the save_atr and save_act functions to update or create a new entry
        RowCount = Sheet.row_count
        if RowCount >1:   
            Existing = Sheet.col_values(1)
        if len(OtherActSheets)==2:#used only by save_act and when true old entries of activities are deleted in all sheets not just the one being saved in
            OtherEx1 = OtherActSheets[0].col_values(1)
            OtherEx2 = OtherActSheets[1].col_values(1)
        saveCount = 0
        
        while saveCount<len(IdList):
            Id = IdList[saveCount]
            Values = ValList[saveCount]
            ##first delete acts in other sheets with same ID
            if len(OtherActSheets)==2:#used only by save_act and when true old entries of activities are deleted in all sheets not just the one being saved in
                print("deleting duplicates")
                RC1 = OtherActSheets[0].row_count
                RC2 = OtherActSheets[1].row_count
                count = 1
                while count<RC1:
                    if OtherEx1[count]==Id:
                        OtherActSheets[0].delete_rows(count+1)
                        OtherEx1.pop(count)
                        break
                    count = count+1
                count = 1
                while count<RC2:
                    if OtherEx2[count]==Id:
                        OtherActSheets[1].delete_rows(count+1)
                        OtherEx2.pop(count)
                        break
                    count = count+1
            if RowCount>1: 
                count = 1
                RowNr = 0
                while count<RowCount:
                    if Existing[count] == Id:
                        RowNr = count+1
                        break
                    count=count+1
                if RowNr ==0:
                    Sheet.append_row(values = Values)
                else:
                    Sheet.update(str(RowNr)+':'+str(RowNr),[Values])
            else:
                Sheet.append_row(values = Values)        
                format_cell_range(Sheet, '2', cellFormat(textFormat=textFormat(bold=False)))
            saveCount = saveCount+1
            
def save_activities(BioOrExeOrSched, ss, JsonMsg, deleteDuplicates):  #to save activities (bio, exe and sched), deleteDuplicates arg is True if the update call was used iso save, which will look in all activity sheets for an act id and delete the old versions
    global ErrorM
    try:
        OtherSheets = []
        if BioOrExeOrSched == "Sched":
            Sheet = ss.worksheet("Schedule")
            if deleteDuplicates==True:
                OtherSheets = [ss.worksheet("Biography"),ss.worksheet("Execution")]
        else:
            if BioOrExeOrSched =="Exe":
                Sheet = ss.worksheet("Execution")
                if deleteDuplicates==True:
                    OtherSheets = [ss.worksheet("Biography"),ss.worksheet("Schedule")]
            else:
                Sheet = ss.worksheet("Biography")
                if deleteDuplicates==True:
                    OtherSheets = [ss.worksheet("Execution"),ss.worksheet("Schedule")]
        DecodedJson = json.loads(JsonMsg)
        Acts = DecodedJson["Activities"]
        ActList = []
        IdList = []
        for key in Acts.keys():
            Act = Acts[key]
            ActId = Act["id"]
            ActType = Act["type"]
            Tsched = Act["tsched"]
            Tstart = Act["tstart"]
            Tend = Act["tend"]
            Stage = Act["stage"]
            S1dataRaw = Act["s1data"]
            S1data = convert_to_string_if_array(S1dataRaw)            
            if BioOrExeOrSched == "Sched":              
                Values = [ActId, ActType, Tsched, Tstart, Tend, Stage, S1data]
                ActList.append(Values)
                IdList.append(ActId)
            else:
                if BioOrExeOrSched == "Exe": 
                    S2data = convert_to_string_if_array(Act["s2data"])
                    Values = [ActId, ActType, Tsched, Tstart, Tend, Stage, S1data, S2data]
                    ActList.append(Values)
                    IdList.append(ActId)
                else:  
                    S2data = convert_to_string_if_array(Act["s2data"])
                    S3data = convert_to_string_if_array(Act["s3data"])
                    Values = [ActId, ActType, Tsched, Tstart, Tend, Stage, S1data, S2data, S3data]
                    ActList.append(Values)
                    IdList.append(ActId)          
        update_or_create(Sheet, IdList, ActList, OtherSheets)
        print('done saving activities')
        Reply = "success"
    except:
        print('except')
        ErrorM.set("ERROR: could not save an activity")
        Reply = "error"
    return Reply 

def save_atr(ss, JsonMsg):#to save attributes
    global ErrorM
    try:
        AtrSheet = ss.worksheet("Attributes")
        DecodedJson = json.loads(JsonMsg)
        Atrs = DecodedJson["Attributes"]
        AtrList = []
        IdList = []
        for key in Atrs.keys():
            Atr = Atrs[key]
            AtrId = Atr["id"]
            AtrType = Atr["type"]
            AtrContext = Atr["context"]
            AtrValue = str(Atr["value"])
            Values = [AtrId, AtrType, AtrContext, AtrValue]
            AtrList.append(Values)
            IdList.append(AtrId)
        update_or_create(AtrSheet, IdList, AtrList,[])
        print('done saving attributes')
        Reply = "success"
    except:
        print("exception")
        ErrorM.set("ERROR: could not save an activity")
        Reply = "error"
    return Reply  

def open_sheet(JsonMsg, GetOrSave):#used to find an existing spreadsheet or create a new one for a holon
    global ErrorM
    global client
    global Emails
    ss = False
    DecodedJson = json.loads(JsonMsg)
    HolonName = DecodedJson["HolonName"]
    HolonType = DecodedJson["HolonType"]
    myThreadId = threading.get_ident()
    ########the following lines of code are used to ensure the same spreadsheet is not edited by more than one python thread at the same time
    if ClashPreventerList == []:
        ClashPreventerList.append([myThreadId,HolonName,HolonType])
    else:
        for c in ClashPreventerList:
            HN = c[1]
            HT = c[2]
            if HN == HolonName and HT == HolonType:
                print("Already opened")
                ss = "busy"
            else:
                ClashPreventerList.append([myThreadId,HolonName,HolonType])
                break
    ##################################################
    Filename = HolonName +"("+HolonType+")"  #each spreadsheet's title is a combination of the holon's name and its type
    if ss ==False:
        try:
            ss = client.open(Filename)  
            print("Opened sheet")
        except:
            if GetOrSave == "save":
                try:
                    ss = client.create(Filename)
                    for E in Emails:
                        try:                       
                            ss.share(E, perm_type = 'user', role = 'reader') ###this is so the user of the service can observe their factory's holon's data without being able to edit it
                        except:
                            print("Please provide a valid email")
                        ErrorM.set("ERROR: provide a valid email to share the holon's data to")
                    #each spreadsheet contains four worksheets for each type of base data to be stored
                    AtrSheet = ss.add_worksheet(title = "Attributes", rows = "1",cols="1")
                    BioSheet = ss.add_worksheet(title = "Biography", rows = "1", cols = "9")
                    SchedSheet = ss.add_worksheet(title = "Schedule", rows = "1", cols = "7")
                    ExeSheet = ss.add_worksheet(title = "Execution", rows = "1", cols = "8")
                    AtrSheet.append_row(values = ['AtrId', 'AtrType', 'Context', 'Value'])
                    BioSheet.append_row(values = ['ActId', 'ActType', 'Tsched', 'Tstart', 'Tend', 'Stage', 'S1data', 'S2data', 'S3data'])
                    SchedSheet.append_row(values = ['ActId', 'ActType', 'Tsched', 'Tstart', 'Tend', 'Stage', 'S1data'])
                    ExeSheet.append_row(values = ['ActId', 'ActType', 'Tsched', 'Tstart', 'Tend', 'Stage', 'S1data', 'S2data'])
                    format_cell_range(AtrSheet, '1',cellFormat(textFormat=textFormat(bold=True)))
                    format_cell_range(BioSheet,'1',cellFormat(textFormat=textFormat(bold=True)))
                    format_cell_range(SchedSheet,'1',cellFormat(textFormat=textFormat(bold=True)))
                    format_cell_range(ExeSheet, '1', cellFormat(textFormat=textFormat(bold=True)))
                    try:
                        ss.del_worksheet('Sheet1')
                    except:
                        print("Sheet 1 does not exist")
                    print("Created new sheet")
                except:
                    print("Could not open or create new sheet")  
                    ErrorM.set("ERROR: A google sheet could not be opened or created")
            else:
                print("Could not open sheet to get values")
    return ss

def listen_and_connect(conn):   #this function is used to start multiple threads that each handle a single request from erlang
    global NrProcesses
    global GlobalLogStrs
    global ErrorM
    StartT = time.time() #for logging purposes
    Performance = "pending" #for logging purposes
    nrProcStart = str(NrProcesses.get()) #for logging purposes
    message = ""
    while True:
        data = conn.recv(1)
        newchar = data.decode("utf-8")
        if newchar =="!": #! is used as a end of request character
            break
        message = message+newchar
    outM = "unknownRequest!" #to be changed if a valid request was received    
    #######Save Requests
    if CredentialsFilePresent:
        if((message == "request_bio_save")or(message=="request_atr_save")or(message=="request_sched_save")or(message=="request_exe_save")or(message == "request_bio_update")or(message=="request_atr_update")or(message=="request_sched_update")or(message=="request_exe_update")or(message =="get_bio")or(message=="get_atr")or(message=="get_sched")or(message=="get_exe")or(message == "get_act")or(message =="del_bio")or(message=="del_atr")or(message=="del_sched")or(message=="del_exe")or(message == "del_act")or(message =="pop_bio")or(message=="pop_atr")or(message=="pop_sched")or(message=="pop_exe")or(message == "pop_act")):
            outM = "PythonReady!"
            conn.sendall(outM.encode("utf-8"))
            Content = ""
            while True:
                data = conn.recv(1)
                newchar = data.decode("utf-8")
                if newchar =='!': #! is used as a end of request character
                    try:
                        if(message[0:7]=="request"): # all save requests start with "request"
                            ss = open_sheet(Content, "save")
                        else:
                            ss = open_sheet(Content, "get")
                        if ss != False and ss!="busy": #if spreadsheet exists and is not being edited by another thread continue
                            if(message == "request_bio_save"):
                                Reply = save_activities("Bio",ss,Content,False)
                                ReqType = "save"#for logging purposes
                                ReqGroup = "biography"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("Activities", Content)#for logging purposes
                            if(message=="request_atr_save"):
                                Reply = save_atr(ss,Content)
                                ReqType = "save"#for logging purposes
                                ReqGroup = "attributes"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("Attributes", Content)#for logging purposes
                            if(message=="request_exe_save"):
                                Reply = save_activities("Exe",ss,Content,False)
                                ReqType = "save"#for logging purposes
                                ReqGroup = "execution"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("Activities", Content)#for logging purposes
                            if(message=="request_sched_save"):
                                Reply = save_activities("Sched",ss,Content,False)
                                ReqType = "save"#for logging purposes
                                ReqGroup = "schedule"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("Activities", Content)#for logging purposes
                            if(message == "request_bio_update"):
                                Reply = save_activities("Bio",ss,Content,True)
                                ReqType = "update"#for logging purposes
                                ReqGroup = "biography"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("Activities", Content)#for logging purposes
                            if(message=="request_atr_update"):
                                Reply = save_atr(ss,Content)
                                ReqType = "update"#for logging purposes
                                ReqGroup = "attributes"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("Attributes", Content)#for logging purposes
                            if(message=="request_exe_update"):
                                Reply = save_activities("Exe",ss,Content,True)
                                ReqType = "update"#for logging purposes
                                ReqGroup = "execution"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("Activities", Content)#for logging purposes
                            if(message=="request_sched_update"):
                                Reply = save_activities("Sched",ss,Content,True)
                                ReqType = "update"#for logging purposes
                                ReqGroup = "schedule"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("Activities", Content)#for logging purposes
                            ##for get request Content contains the get filter: for Atr: all|id|type|context; for Act: all|id|type|tsched(before,beforeAndOn, on, afterAndOn,after)|tstart(same)|tend(same)
                            if(message=="get_atr"):
                                Reply = find("Atr",ss, Content, "get")
                                ReqType = "get"#for logging purposes
                                ReqGroup = "attributes"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("",Reply)#for logging purposes
                            if(message == "get_bio"):
                                Reply = find("Bio",ss,Content, "get")
                                ReqType = "get"#for logging purposes
                                ReqGroup = "biography"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("",Reply)#for logging purposes
                            if(message == "get_sched"):
                                Reply = find("Sched",ss, Content, "get")
                                ReqType = "get"#for logging purposes
                                ReqGroup = "schedule"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("",Reply)#for logging purposes
                            if(message == "get_exe"):
                                Reply = find("Exe",ss, Content, "get")
                                ReqType = "get"#for logging purposes
                                ReqGroup = "execution"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("",Reply)#for logging purposes
                            if(message == "get_act"):
                                Reply = find("Act",ss,Content, "get")##this act can be in any of act sheets and requester does not know which
                             ##for get request Content contains the get filter: for Atr: all|id|type|context; for Act: all|id|type|tsched(before,beforeAndOn, on, afterAndOn,after)|tstart(same)|tend(same)
                                ReqType = "get"#for logging purposes
                                ReqGroup = "schedule"#for logging purposes
                                NrAtrsOrActs = getNrAtrsOrActs("",Reply)#for logging purposes
                            if(message=="del_atr"):
                                Reply = find("Atr",ss, Content, "del")
                                ReqType = "delete"#for logging purposes
                                ReqGroup = "attributes"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            if(message == "del_bio"):
                                Reply = find("Bio",ss,Content, "del")
                                ReqType = "delete"#for logging purposes
                                ReqGroup = "biography"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            if(message == "del_sched"):
                                Reply = find("Sched",ss, Content, "del")
                                ReqType = "delete"#for logging purposes
                                ReqGroup = "schedule"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            if(message == "del_exe"):
                                Reply = find("Exe",ss, Content, "del")
                                ReqType = "delete"#for logging purposes
                                ReqGroup = "execution"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            if(message == "del_act"):
                                Reply = find("Act",ss,Content, "del")
                                ReqType = "delete"#for logging purposes
                                ReqGroup = "schedule"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            if(message=="pop_atr"):
                                Reply = find("Atr",ss, Content, "pop")
                                ReqType = "pop"#for logging purposes
                                ReqGroup = "attributes"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            if(message == "pop_bio"):
                                Reply = find("Bio",ss,Content, "pop")
                                ReqType = "pop"#for logging purposes
                                ReqGroup = "biography"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            if(message == "pop_sched"):
                                Reply = find("Sched",ss, Content, "pop")
                                ReqType = "pop"#for logging purposes
                                ReqGroup = "schedule"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            if(message == "pop_exe"):
                                Reply = find("Exe",ss, Content, "pop")
                                ReqType = "pop"#for logging purposes
                                ReqGroup = "execution"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            if(message == "pop_act"):
                                Reply = find("Act",ss,Content, "pop")
                                ReqType = "pop"#for logging purposes
                                ReqGroup = "schedule"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                            Performance = "success"
                        else:
                            if ss=="busy":
                                Reply = "The google sheet for this holon is already being edited by another process"
                                ReqType = "unknown"#for logging purposes
                                ReqGroup = "unknown"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                                Performance = "busy"#for logging purposes
                            else:
                                Reply = "error"
                                ReqType = "unknown"#for logging purposes
                                ReqGroup = "unknown"#for logging purposes
                                NrAtrsOrActs = 0#for logging purposes
                                Performance = "invalid request"#for logging purposes
                                
                    except:
                        Reply = "error"
                        ReqType = "unknown"#for logging purposes
                        ReqGroup = "unknown"#for logging purposes
                        NrAtrsOrActs = 0#for logging purposes
                        Performance = "failed"#for logging purposes
                    Reply = Reply +"!"   #erlang sees ! as a end of reply character
                    remove_from_clash_list(Content) #the spreadsheet of the holon whos data was saved is not being edited anymore
                    conn.sendall(Reply.encode("utf-8"))
                    break
                Content = Content+newchar
        else:
            ErrorM.set("ERROR: Unknown request received from erlang")
    else:
        print("Replying: Google Credentials file not present")
        outM = "Google Credentials file not present!"
        conn.sendall(outM.encode("utf-8"))
    conn.close()
    nrProcEnd = str(NrProcesses.get()-1)#for logging purposes
    NrProcesses.set(NrProcesses.get()-1)#displayed on GUI
    NrProcessesStr.set("Number of active erlang requests being handled: %d" %NrProcesses.get())#displayed on GUI
    ExecutionTime = time.time()-StartT#for logging purposes
    LogStr = [str(datetime.datetime.now()),ReqType,ReqGroup,Content,str(NrAtrsOrActs),nrProcStart,nrProcEnd,str(ExecutionTime),Performance]#for logging purposes
    GlobalLogStrs.append(LogStr)#for logging purposes
    threading.current_thread().kill()#kill this thread after request has been handled

def log_history(): #this is a secondary thread running for ever that logs all requests in a local excel file
    global GlobalLogStrs
    global mysheet
    global myworkbook
    global LoggerInfo
    global ErrorM
    if LoggerInfo.get() == "Excel Logger status: Working":
        while True:        
            if len(GlobalLogStrs)>0:
                try:
                    print("logging")
                    for L in GlobalLogStrs:
                        mysheet.append(L)   
                        GlobalLogStrs.remove(L)
                    myworkbook.save('GoogleSheetPythonLogger.xlsx')
                    print("logger updated")
                except:
                    print("Could not log new data into excel")
                    LoggerInfo.set("Excel Logger status: Could not save to excel sheet. Close the sheet if open")
                    ErrorM.set("ERROR: Please close excel logger sheet")
            time.sleep(10)

def MainThread():#this is the main thread that runs forever waiting for new requests from erlang
    global TcpInfo
    global NrProcesses
    global NrProcessesStr
    global GlobalLogStrs
    global BusySaving
    global ErrorM
    while CredentialsFilePresent==False:
        time.sleep(5)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            s.bind((HOST, PORT))
            TcpInfo.set("Python tcp port on localhost: 11000" ) # to be displayed on the GUI
        except:# if for some bad luck some other process on the computer running this program is already using tcp port 11 000, python gets any available port number to open a port on and prints the port number on the gui, so the user can change the tcp address in erlang as well
            s.bind(HOST, 0)
            PortNr = s.getsockname()[1]
            ErrorM.set("ERROR: TCP port 11000 not available. Edit erlang code to use port %d", PortNr) # to be displayed on the GUI
            TcpInfo.set("Python tcp port on localhost: %d", PortNr ) # to be displayed on the GUI
        while True:
            s.listen()
            conn, addr = s.accept()
            print('Google sheet saver connected by', addr)
            x = thread_with_trace(target=listen_and_connect, args=(conn,))
            x.start()
            NrProcesses.set(NrProcesses.get()+1) # to be displayed on the GUI
            NrProcessesStr.set("Number of active erlang requests being handled: %d" %NrProcesses.get()) # to be displayed on the GUI
            y = threading.Thread(target=raise_timeout_exception,args=(20, x,)) #a seperate thread is started for each incoming request so the server can handle multiple requests at the same time
            y.start()
def add_email():#this function is called when the user clicks the 'Add email' button on the gui 
    global Emails
    global UserEmail
    print(len(Emails))
    Emails.append(UserEmail.get())
    f = open("emails.txt", "w")
    string = Emails[0]
    count = 1
    while count<len(Emails):
        string = string+","+Emails[count]
        count=count+1
    f.write(string)
    f.close()
    UserEmail.set("Successfully added. Enter a email address and click add or remove. All added emails receive all holon's spreadsheet data to view")
def remove_email():#this function is called when the user clicks the 'Remove email' button on the gui
    global Emails
    global UserEmail
    f = open("emails.txt", "w")
    for e in Emails:
        if UserEmail.get()=="all" or UserEmail.get()==e:
            Emails.remove(e)
    if len(Emails)>0:
        string = Emails[0]
        count = 1
        while count<len(Emails):
            string = string+","+Emails[count]
            count=count+1
    else:
        string = ""
    f.write(string)        
    f.close()
    UserEmail.set("Successfully removed. Enter a email address and click add or remove. All added emails receive all holon's spreadsheet data to view")   
def find_credentials(): #this function is called when the user clicks the 'Find Google Credentials' button on the gui: it takes the full address (directory and filename.json) entered into the entry box above the button and looks checks if this is a valid google credentials file
    global CredsLoc
    global CredentialsFilePresent
    global CredentialsPresent
    global client
    global GoogleCredsBtn
    global GoogleCredsLocEntry
    global ErrorM
    try:
        creds = ServiceAccountCredentials.from_json_keyfile_name(CredsLoc.get(), scope)
        client = gspread.authorize(creds)
        CredentialsFilePresent = True
        CredentialsPresent.set("Google Credentials present: True")
        GoogleCredsBtn.destroy()
        GoogleCredsLocEntry.destroy()      
    except:
        print("Credentials file not loaded")
        CredentialsPresent.set("Google Credentials present: False")
        ErrorM.set("Credentials file not found")
        
########Program starts here and calls functions declared above#############
########Tkinter window for the GUI#########      
root = tk.Tk()  
root.title("Python Server for Google Sheet Saver") 
root.geometry("800x400") 

#########Variables used in the GUI
TcpInfo = tk.StringVar(root)
CredentialsPresent = tk.StringVar(root)
NrProcesses = tk.IntVar(root)
NrProcesses.set(0)
NrProcessesStr = tk.StringVar(root)
LoggerInfo = tk.StringVar(root)
ErrorM = tk.StringVar(root)
CredsLoc = tk.StringVar(root)
CredsLoc.set("Enter location of google credentials + credentials file name. Example: c:/Desktop/Credentials/MyCredentials.json")
UserEmail = tk.StringVar(root)
UserEmail.set("Enter a email address and click add or remove. All added emails receive all holon's spreadsheet data to view")
###############Other global variables####################
GlobalLogStrs = [] #used by log_history thread
ClashPreventerList = [] #used to ensure a spreadsheet is not edited by two threads at the same time, i.e. for the same holon cause each holon has their own spreadsheet
HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 11000        # Port to listen on (non-privileged ports are > 1023)
Emails = []
####Variables needed for connection to Google Sheets#####
scope = ["https://spreadsheets.google.com/feeds",'https://www.googleapis.com/auth/spreadsheets',"https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]
CredentialsFilePresent = False
CredentialsPresent.set("Google Credentials present: False")
client = "None"    

################Pack GUI elements##################   
GoogleCredsLocEntry = tk.Entry(root, textvariable = CredsLoc)
GoogleCredsLocEntry.pack(fill = tk.X)
GoogleCredsBtn = tk.Button(root, text = "Find Google Credentials")
GoogleCredsBtn.config(command = find_credentials)
GoogleCredsBtn.pack()
EmailLbl = tk.Label(root, text = "Email: (enter 'all' if you want to remove all emails and then click Remove email)").pack()
EmailEntry = tk.Entry(root, textvariable = UserEmail)
EmailEntry.pack(fill = tk.X)
EmailAddBtn = tk.Button(root, text = "Add email")
EmailAddBtn.config(command = add_email)
EmailAddBtn.pack()
EmailRemBtn = tk.Button(root, text = "Remove email")
EmailRemBtn.config(command = remove_email)
EmailRemBtn.pack()
TcpLbl = tk.Label(root, textvariable = TcpInfo).pack()#gives info on tcp server's status
CredLbl = tk.Label(root, textvariable = CredentialsPresent).pack() #says if valid google credentials are present
LoggerLbl = tk.Label(root, textvariable = LoggerInfo).pack() #says if logging is successful or if excel is missing or being used by user in which case logging is put on hold
NrProcLbl = tk.Label(root, textvariable = NrProcessesStr).pack() #states the number of erlang requests being handled at the present time
ErrorMLbl = tk.Label(root, textvariable = ErrorM).pack() #states the most important error message at some point in time (normally not more than 1 anyways)

############Open Excel Logger################
try:
    myworkbook = openpyxl.load_workbook('GoogleSheetPythonLogger.xlsx')
    mysheet = myworkbook['Sheet1']
    LoggerInfo.set("Excel Logger status: Working")
except:
    print("Could not open excel logger")
    LoggerInfo.set("Excel Logger status: Could not find excel sheet")
#################end####################
 
###########Get existing emails to share to############
f = open("emails.txt", "r")
Content = f.read()
Content.replace("\n", "")
Content.replace(" ", "")
if Content!="":
    Emails = Content.split(",")
####################end###################

########Start main thread listening for new requests from erlang########
MT = threading.Thread(target = MainThread,name='MainThread',daemon=True)
MT.start()
###############################end######################################

#######Start logging thread responsible for logging all requests in an excel file#########
LT = threading.Thread(target=log_history)               
LT.start() 
####################################################end###################################

##########################Start GUI########################################
root.mainloop()           
        
